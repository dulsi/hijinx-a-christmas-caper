extends Node

static func load_geometry(geometry_string : String, geometry_width : int, geometry_length : int, geometry : Node) -> void:
	# Fix seed
	seed(10)
	# First, convert the string into an array and decompress it
	var geometry_array : PoolByteArray = Marshalls.base64_to_variant(geometry_string)
	geometry_array = geometry_array.decompress(geometry_length, Constants.COMPRESSION_TYPE)
	# Now apply the contents of the array to the room geometry
	for i in range(0, geometry_array.size()):
		# Figure out the tile position
		var tile_position : Vector2 = Vector2(i % geometry_width, i / geometry_width)
		# Now add the tile to the room
		var tile_type : int = geometry_array[i]
		match tile_type:
			Constants.LEVELTILE_DIRT:
				geometry.add_dirt_tile(tile_position)
			Constants.LEVELTILE_ICE:
				geometry.add_ice_tile(tile_position)
			Constants.LEVELTILE_BRICK:
				geometry.add_brick_tile(tile_position)
	geometry.update_bitmasks()
	geometry.update_snow_tiles()

static func add_object(object_data : Dictionary, geometry : Node) -> void:
	match object_data["type"]:
		Constants.EDITOR_ITEM_NAME_SPIKE:
			geometry.add_spike(
				Vector2(object_data["position"][0], object_data["position"][1]),
				object_data["frame"]
			)
		Constants.EDITOR_ITEM_NAME_SPARK:
			geometry.add_spark(
				Vector2(object_data["position"][0], object_data["position"][1]),
				object_data["anticlockwise"]
			)
		Constants.EDITOR_ITEM_NAME_MOVING_PLATFORM:
			geometry.add_moving_platform(
				Vector2(object_data["position"][0], object_data["position"][1]),
				Vector2(object_data["direction"][0], object_data["direction"][1])
			)
		Constants.EDITOR_ITEM_NAME_ORB:
			geometry.add_orb(
				Vector2(object_data["position"][0], object_data["position"][1]),
				object_data["colour"]
			)
		Constants.EDITOR_ITEM_NAME_HAMMER:
			geometry.add_hammer(
				Vector2(object_data["position"][0], object_data["position"][1]),
				object_data["starting_delay"]
			)
		Constants.EDITOR_ITEM_NAME_SPRINGBOARD_PLATFORM:
			geometry.add_springboard_platform(
				Vector2(object_data["position"][0], object_data["position"][1]),
				Vector2(object_data["direction"][0], object_data["direction"][1])
			)
		Constants.EDITOR_ITEM_NAME_TURRET:
			geometry.add_turret(
				Vector2(object_data["position"][0], object_data["position"][1]),
				Vector2(object_data["firing_direction"][0], object_data["firing_direction"][1]),
				object_data["firing_interval"]
			)
		Constants.EDITOR_ITEM_NAME_CARL:
			geometry.add_carl(
				Vector2(object_data["position"][0], object_data["position"][1]),
				object_data["conversation"]
			)
		_:
			geometry.add_object(
				Vector2(object_data["position"][0], object_data["position"][1]),
				object_data["type"]
			)

static func load_room(room_data : Dictionary, geometry : Node) -> void:
	load_geometry(room_data["geometry"], room_data["geometry_width"], room_data["geometry_length"], geometry)
	var player_spawn_pos : Vector2 = Vector2(room_data["player_spawn_position"][0], room_data["player_spawn_position"][1])
	geometry.add_player(player_spawn_pos)
	for object in room_data["objects"]:
		add_object(object, geometry)
	# A quick fix - we're introducing flags partway through, so let's check for levels without them
	if not room_data.has("flags"):
		room_data["flags"] = []
	# Look at flags
	if Constants.LEVEL_PROPERTY_NAME_GOAL_HIDDEN_BY_ORBS in room_data["flags"]:
		geometry.add_orb_controller()
	# Massively janky hack time!
	if Constants.LEVEL_PROPERTY_NAME_CONVO_SCENE in room_data["flags"]:
		for node in geometry.get_tree().get_nodes_in_group("interactable"):
			if node is Carl:
				node.auto_talk = true
