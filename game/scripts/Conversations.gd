extends Node

const FLAG_LINE_SLOW : String = "slow"
const FLAG_LINE_FAST : String = "fast"
const FLAG_LINE_AUTO : String = "auto"
const FLAG_LINE_NO_MUSIC : String = "no_music"
const FLAG_LINE_MUSIC_CUE_A : String = "music_cue_a"
const FLAG_CONVO_EPILOGUE : String = "epilogue"

var conversations : Dictionary

func list_conversations() -> Array:
	return conversations.keys()

func list_optional_conversations() -> Array:
	var results : Array = []
	for convo_name in list_conversations():
		if not convo_name.begins_with("end_"): # janky hack, m8!
			results.append(convo_name)
	return results

func get_line_in_conversation(conversation_name : String, line_number : int) -> Dictionary:
	return conversations[conversation_name]["lines"][line_number]

func get_line_count_in_conversation(conversation_name : String) -> int:
	return conversations[conversation_name]["lines"].size()

func get_flags_in_conversation(conversation_name : String) -> Array:
	if conversations[conversation_name].has("flags"):
		return conversations[conversation_name]["flags"]
	else:
		return []

func conversation_has_flag(conversation_name : String, flag : String) -> bool:
	return get_flags_in_conversation(conversation_name).has(flag)

func does_conversation_have_another_line(conversation_name : String, current_line : int) -> bool:
	return get_line_count_in_conversation(conversation_name) - 1 >= current_line

func get_dialogue_in_line(conversation_name : String, line_number : int) -> String:
	return get_line_in_conversation(conversation_name, line_number)["line"]

func get_speaker_for_line(conversation_name : String, line_number : int) -> String:
	return get_line_in_conversation(conversation_name, line_number)["speaker"]

func get_expression_for_line(conversation_name : String, line_number : int) -> int:
	return get_line_in_conversation(conversation_name, line_number)["expression"]

func get_flags_in_line(conversation_name : String, line_number : int) -> Array:
	var line_data : Dictionary = get_line_in_conversation(conversation_name, line_number)
	if line_data.has("flags"):
		return line_data["flags"]
	else:
		return []

func line_has_flag(conversation_name : String, line_number : int, flag : String):
	return get_flags_in_line(conversation_name, line_number).has(flag)

func _ready() -> void:
	conversations = Utilities.load_data_from_json("res://data/conversations.json")["conversations"]
