extends Node

const SAVE_PATH : String = "user://save.json"

const DEFAULT_LEVEL_PROGRESS : Dictionary = {
	"unlocked": false,
	"cleared": false,
	"got_rings": false,
	"best_time": -1
}

var level_progress : Dictionary
var sets_unlocked : Array
var conversations_had : Array
var achievements_unlocked : Array
var new_game : bool
var gzilla = preload("res://gdgamerzillascript.gdns").new()
var achievements : Dictionary

func get_level_progress(level_name : String) -> Dictionary:
	if level_progress.has(level_name):
		return level_progress[level_name]
	else:
		return DEFAULT_LEVEL_PROGRESS

func is_level_unlocked(level_name : String) -> bool:
	return get_level_progress(level_name)["unlocked"]

func is_level_cleared(level_name : String) -> bool:
	return get_level_progress(level_name)["cleared"]

func is_level_cleared_with_rings(level_name : String) -> bool:
	return get_level_progress(level_name)["got_rings"]

func get_best_time_for_level(level_name : String) -> int:
	return get_level_progress(level_name)["best_time"]

func set_level_unlocked(level_name : String) -> void:
	if not level_progress.has(level_name):
		level_progress[level_name] = DEFAULT_LEVEL_PROGRESS.duplicate()
	level_progress[level_name]["unlocked"] = true

func set_level_cleared(level_name : String) -> void:
	if not level_progress.has(level_name):
		level_progress[level_name] = DEFAULT_LEVEL_PROGRESS.duplicate()
	level_progress[level_name]["cleared"] = true
	new_game = false

func set_level_rings_collected(level_name : String) -> void:
	if not level_progress.has(level_name):
		level_progress[level_name] = DEFAULT_LEVEL_PROGRESS.duplicate()
	level_progress[level_name]["got_rings"] = true

func set_level_best_time(level_name : String, best_time : int) -> void:
	if not level_progress.has(level_name):
		level_progress[level_name] = DEFAULT_LEVEL_PROGRESS.duplicate()
	var current_best : int = level_progress[level_name]["best_time"]
	if current_best == -1 or best_time < current_best:
		level_progress[level_name]["best_time"] = best_time

func is_set_unlocked(set_name : String) -> bool:
	return sets_unlocked.has(set_name)

func unlock_set(set_name : String) -> void:
	if not sets_unlocked.has(set_name):
		sets_unlocked.append(set_name)
		# Also unlock the first level in the new set
		var level_name : String = Levels.get_level_in_set(set_name, 0)
		set_level_unlocked(level_name)

func get_completion_percentage() -> float:
	var tokens : int = 0
	for level_name in level_progress:
		# janky hack m8
		if "_end" in level_name:
			continue
		if is_level_cleared(level_name):
			tokens += 1
		if is_level_cleared_with_rings(level_name):
			tokens += 1
		if GameProgress.get_best_time_for_level(level_name) <= Levels.get_level_par(level_name) and GameProgress.get_best_time_for_level(level_name) != -1:
			tokens += 1
	return float(tokens) / 72.0 # ew, hard-coded stuff!

func is_achievement_unlocked(achievement_name : String) -> bool:
	return achievements_unlocked.has(achievement_name)

func set_achievement_unlocked(achievement_name : String) -> void:
	if not achievements_unlocked.has(achievement_name):
		achievements_unlocked.append(achievement_name)
	if (gzilla):
		gzilla.setTrophy(achievements[achievement_name]["name"])

func get_achievement_unlocked_count() -> int:
	return achievements_unlocked.size()

func set_conversation_had(conversation_name : String) -> void:
	if not conversations_had.has(conversation_name):
		conversations_had.append(conversation_name)

func save_exists() -> bool:
	var file : File = File.new()
	return file.file_exists(SAVE_PATH)

func save_game() -> void:
	var json : Dictionary = {
		"level_progress": level_progress,
		"sets_unlocked": sets_unlocked,
		"conversations_had": conversations_had,
		"achievements_unlocked": achievements_unlocked,
		"new_game": new_game
	}
	var content : String = to_json(json)
	var file : File = File.new()
	file.open(SAVE_PATH, File.WRITE)
	file.store_string(content)
	file.close()

func load_game() -> void:
	var json : Dictionary = Utilities.load_data_from_json(SAVE_PATH)
	level_progress = json["level_progress"]
	sets_unlocked = json["sets_unlocked"]
	conversations_had = json["conversations_had"]
	achievements_unlocked = json["achievements_unlocked"]
	new_game = json["new_game"]

func new_game() -> void:
	level_progress = {}
	sets_unlocked = []
	conversations_had = []
	achievements_unlocked = []
	new_game = true
	set_level_unlocked("a1")
	unlock_set("a")
	save_game()

func _ready() -> void:
	if (gzilla):
		gzilla.start(false, OS.get_user_data_dir())
		gzilla.setGameFromFile("res://gamerzilla/hijinx_christmas_caper.game.tres", "")
		achievements = Utilities.load_data_from_json("res://data/achievements.json")
	if save_exists():
		load_game()
	else:
		new_game()
