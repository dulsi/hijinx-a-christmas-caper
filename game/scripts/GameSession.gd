extends Node

var playtest_data : Dictionary # Room data goes here when playtesting a level
var playtesting : bool = false # Flag used so the editor knows if we're returning from a playtest session

var selected_set : String
var selected_level : int
