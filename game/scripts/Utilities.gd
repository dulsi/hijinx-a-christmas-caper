extends Node

func time_to_string(time : int) -> String:
	var milliseconds : int = time % 1000
	var seconds : int = int(floor(float(time) / 1000.0)) % 60
	var minutes : int = floor(float(time) / 60000.0)
	return "%02d:%02d.%03d" % [minutes, seconds, milliseconds]

func load_data_from_json(path : String) -> Dictionary:
	var file : File = File.new()
	file.open(path, File.READ)
	var contents : String = file.get_as_text()
	file.close()
	var json : Dictionary = parse_json(contents)
	return json
