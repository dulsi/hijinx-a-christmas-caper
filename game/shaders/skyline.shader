shader_type canvas_item;

const float SKYLINE_WIDTH = 352.0;
const float SKYLINE_HEIGHT = 96.0;

uniform sampler2D noise_texture;
uniform sampler2D dither_texture;
uniform sampler2D palette_texture;

uniform int which_palette = 0;

vec2 pixelate(vec2 uv) {
	return vec2(round(uv.x * SKYLINE_WIDTH) / SKYLINE_WIDTH, round(uv.y * SKYLINE_HEIGHT) / SKYLINE_HEIGHT);
}

void fragment() {
	vec2 uv_p = pixelate(UV);
	float level = texture(TEXTURE, uv_p).r;
	float dither = texture(dither_texture, UV * vec2(160.0, 48.0)).r;
	vec2 uv_a = uv_p + vec2(TIME / 20.0f, 0.0);
	vec2 uv_b = uv_p - vec2(TIME / 80.0f, 0.0);
	float noise_a = texture(noise_texture, uv_a).r;
	float noise_b = texture(noise_texture, uv_b).g;
	float noise_c = (noise_a + noise_b) * 0.5f;
	float cloud = (level * noise_c);
	cloud += dither / 20.0f;
	cloud *= 2.5f;
	vec2 palette_uv = vec2(float(which_palette) / 3.0f, 1.0f - cloud);
	COLOR.rgba = texture(palette_texture, palette_uv).rgba;
}