shader_type canvas_item;

void fragment() {
	vec2 normal = texture(NORMAL_TEXTURE, UV).rg;
	normal -= vec2(0.5, 0.5);
	normal *= vec2(2.0, -2.0);
	normal *= SCREEN_PIXEL_SIZE * 128.0;
	normal *= 1.0 - (texture(NORMAL_TEXTURE, UV).b);
	vec2 screen_pos = SCREEN_UV * SCREEN_PIXEL_SIZE * vec2(640.0, 360.0);
	screen_pos += normal;
	screen_pos /= vec2(640.0, 360.0) * SCREEN_PIXEL_SIZE;
	vec4 reflection = texture(SCREEN_TEXTURE, screen_pos);
	// Tint the reflection a bit
	reflection *= vec4(0.5, 0.75, 1.0, 1.0);
	vec4 ice = texture(TEXTURE, UV);
	COLOR = ice;
	COLOR.rgb += reflection.rgb * texture(NORMAL_TEXTURE, UV).b;
}