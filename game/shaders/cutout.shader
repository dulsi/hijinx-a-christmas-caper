shader_type canvas_item;

const vec2 SCREEN_RATIO = vec2(16.0f, 9.0f);
const float TRANSITION_COEFFICIENT = 20.0f;
uniform float transition_amount = 1.0f;
uniform vec2 center_position = vec2(0.5f, 0.5f);

void fragment() {
	vec2 pos = UV * SCREEN_RATIO;
	if (distance(pos, center_position * SCREEN_RATIO) < transition_amount * TRANSITION_COEFFICIENT) {
		COLOR.a = 0.0f;
	}
}
