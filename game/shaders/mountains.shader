shader_type canvas_item;

uniform sampler2D palette_texture;
uniform int which_palette;

void fragment() {
	vec4 shade = texture(TEXTURE, UV);
	COLOR.a = shade.a;
	if (shade.a != 0.0f) {
		vec2 shade_uv = vec2(float(which_palette) / 4.0f, shade.r);
		COLOR.rgb = texture(palette_texture, shade_uv).rgb;
	}
}