extends Node2D

const RoomLoader : GDScript = preload("res://scripts/RoomLoader.gd")

onready var geometry : Node = $Geometry

func player_dead() -> void:
	yield(get_tree().create_timer(1.5), "timeout")
	get_tree().reload_current_scene()

func level_cleared() -> void:
	get_tree().change_scene("res://scenes/LevelEditor.tscn")

func _input(event : InputEvent) -> void:
	if event.is_action("editor_stop"):
		get_tree().change_scene("res://scenes/LevelEditor.tscn")

func _ready() -> void:
	geometry.game = self
	RoomLoader.load_room(GameSession.playtest_data, geometry)
