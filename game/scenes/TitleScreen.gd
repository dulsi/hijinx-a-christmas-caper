extends Control

onready var logo : TextureRect = $Logo
onready var menu : Control = $CanvasLayer/Center/Menu
onready var label_byline : Label = $Label_ByLine
onready var label_version : Label = $Label_Version
onready var level_select : Control = $CanvasLayer/LevelSelect
onready var achievements : Control = $CanvasLayer/Achievements
onready var credits : Control = $CanvasLayer/Credits
onready var cutout : ColorRect = $CanvasLayer/Cutout
onready var background : Node2D = $Background
onready var menu_center : Control = $CanvasLayer/Center
onready var tween : Tween = $Tween

func _ready() -> void:
	menu.items = {
		"main_menu": {
			"type": "menu",
			"children": ["play", "settings", "achievements", "credits", "quit"]
		},
		"play": {
			"type": "button",
			"label": "Play"
		},
		"settings": {
			"label": "Settings",
			"type": "menu",
			"children": ["video", "audio", "controls", "back"],
			"parent": "main_menu"
		},
		"video": {
			"label": "Video",
			"type": "menu",
			"children": ["fullscreen", "show_cursor", "back"],
			"parent": "settings"
		},
		"fullscreen": {
			"label": "Fullscreen",
			"type": "variable",
			"variable_name": "fullscreen"
		},
		"show_cursor": {
			"label": "Show Mouse Cursor",
			"type": "variable",
			"variable_name": "show_cursor"
		},
		"audio": {
			"label": "Audio",
			"type": "menu",
			"children": ["sfx_volume", "bgm_volume", "back"],
			"parent": "settings"
		},
		"sfx_volume": {
			"label": "SFX",
			"type": "variable",
			"variable_name": "sfx_volume"
		},
		"bgm_volume": {
			"label": "Music",
			"type": "variable",
			"variable_name": "bgm_volume"
		},
		"controls": {
			"label": "Controls",
			"type": "menu",
			"children": ["key_binding_run_left", "key_binding_run_right", "key_binding_jump", "key_binding_interact", "key_binding_restart", "key_binding_pause", "back"],
			"parent": "settings"
		},
		"key_binding_run_left": {
			"label": "Run Left",
			"type": "key_binding",
			"action_name": "run_left"
		},
		"key_binding_run_right": {
			"label": "Run Right",
			"type": "key_binding",
			"action_name": "run_right"
		},
		"key_binding_jump": {
			"label": "Jump",
			"type": "key_binding",
			"action_name": "jump"
		},
		"key_binding_interact": {
			"label": "Interact",
			"type": "key_binding",
			"action_name": "interact"
		},
		"key_binding_restart": {
			"label": "Restart Level",
			"type": "key_binding",
			"action_name": "restart"
		},
		"key_binding_pause": {
			"label": "Pause",
			"type": "key_binding",
			"action_name": "pause"
		},
		"achievements": {
			"type": "button",
			"label": "Achievements"
		},
		"credits": {
			"type": "button",
			"label": "Credits"
		},
		"quit": {
			"type": "button",
			"label": "Quit"
		},
		"back": {
			"type": "button",
			"label": "Back"
		}
	}
	menu.variables = {
		"fullscreen": {"type": "tickbox", "value": Settings.fullscreen},
		"show_cursor": {"type": "tickbox", "value": Settings.show_cursor},
		"sfx_volume": {"type": "volume", "value": Settings.sfx_volume},
		"bgm_volume": {"type": "volume", "value": Settings.bgm_volume}
	}
	menu.current_item = "main_menu"
	menu.resize(true)
	MusicController.play_music("Pause Menu Theme")
	cutout.set_center(Vector2(0.5, 0.5))
	cutout.transition_in()
	level_select.connect("level_selected", self, "level_selected")
	level_select.connect("close_level_select", self, "close_level_select")
	achievements.connect("close_achievements", self, "close_achievements")
	credits.connect("close_credits", self, "close_credits")
	background.set_palette(2)
	# In case we're returning from ingame
	get_tree().paused = false

func _on_Menu_variable_changed(variable_name : String) -> void:
	Settings.fullscreen = menu.variables["fullscreen"]["value"]
	Settings.show_cursor = menu.variables["show_cursor"]["value"]
	Settings.sfx_volume = menu.variables["sfx_volume"]["value"]
	Settings.bgm_volume = menu.variables["bgm_volume"]["value"]
	Settings.apply_config()
	Settings.save_config()

func go_to_level(set_name : String, level : int) -> void:
	SoundController.play_sound("ui_start")
	menu.active = false
	MusicController.fade_out_music(1.0)
	cutout.transition_out()
	yield(cutout, "transition_done")
	GameSession.selected_set = set_name
	GameSession.selected_level = level
	get_tree().change_scene("res://scenes/Game.tscn")

func level_selected(set_name : String, level : int) -> void:
	level_select.active = false
	go_to_level(set_name, level)

func open_level_select() -> void:
	SoundController.play_sound("ui_select")
	menu.active = false
	level_select.active = true
	logo.hide()
	menu.hide()
	label_byline.hide()
	label_version.hide()
	level_select.enter_screen()
	level_select.show()

func close_level_select() -> void:
	menu.active = true
	level_select.active = false
	logo.show()
	menu.show()
	label_byline.show()
	label_version.show()
	level_select.hide()

func start_game() -> void:
	if GameProgress.new_game:
		go_to_level("a", 0)
	else:
		open_level_select()

func open_achievements() -> void:
	menu.active = false
	achievements.active = true
	logo.hide()
	menu.hide()
	label_byline.hide()
	label_version.hide()
	achievements.show()

func close_achievements() -> void:
	menu.active = true
	achievements.active = false
	logo.show()
	menu.show()
	label_byline.show()
	label_version.show()
	achievements.hide()

func open_credits() -> void:
	menu.active = false
	credits.active = true
	logo.hide()
	menu.hide()
	label_byline.hide()
	label_version.hide()
	credits.show()

func close_credits() -> void:
	menu.active = true
	credits.active = false
	logo.show()
	menu.show()
	label_byline.show()
	label_version.show()
	credits.hide()

func _on_Menu_button_pressed(slug : String) -> void:
	match slug:
		"play":
			start_game()
		"achievements":
			open_achievements()
		"credits":
			open_credits()
		"quit":
			menu.active = false
			cutout.transition_out()
			yield(cutout, "transition_done")
			get_tree().quit()

func _on_Menu_menu_entered(slug : String) -> void:
	match slug:
		"settings":
			tween.interpolate_property(logo, "rect_position", logo.rect_position, Vector2(160, 120), 0.5, Tween.TRANS_QUINT, Tween.EASE_OUT)
			tween.interpolate_property(menu_center, "rect_position", menu_center.rect_position, Vector2(0, 235), 0.5, Tween.TRANS_QUINT, Tween.EASE_OUT)
			tween.interpolate_property(menu_center, "rect_size", menu_center.rect_size, Vector2(640, 110), 0.5, Tween.TRANS_QUINT, Tween.EASE_OUT)
			tween.start()
		"controls":
			tween.interpolate_property(logo, "rect_position", logo.rect_position, Vector2(160, -360), 0.5, Tween.TRANS_QUINT, Tween.EASE_OUT)
			tween.interpolate_property(menu_center, "rect_position", menu_center.rect_position, Vector2(0, 0), 0.5, Tween.TRANS_QUINT, Tween.EASE_OUT)
			tween.interpolate_property(menu_center, "rect_size", menu_center.rect_size, Vector2(640, 360), 0.5, Tween.TRANS_QUINT, Tween.EASE_OUT)
			tween.start()
