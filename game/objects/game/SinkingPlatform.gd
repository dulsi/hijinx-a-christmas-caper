extends KinematicBody2D

class_name SinkingPlatform

const SINK_SPEED : float = 32.0

onready var sprite : Sprite = $Sprite
onready var sprite_thruster : Sprite = $Sprite/Sprite_Thruster
onready var area_player_stand : Area2D = $Area2D_PlayerStand

var anim_index : float = 0
var sinking : bool = false
onready var original_height : float = position.y

func _process(delta : float) -> void:
	anim_index += delta
	sprite_thruster.frame = wrapi(anim_index * 10.0, 0, sprite_thruster.hframes)
	# If we're sinking, move down
	if sinking:
		position.y += SINK_SPEED * delta
		# If the player is standing here, move them down, too
		for body in area_player_stand.get_overlapping_bodies():
			if body is Player:
				body.position.y += SINK_SPEED * delta
	else:
		position.y = clamp(position.y - SINK_SPEED * delta, original_height, position.y)

func _on_Area2D_PlayerStand_body_entered(body : PhysicsBody2D) -> void:
	if body is Player:
		sinking = true
		SoundController.play_sound("platform_land")

func _on_Area2D_PlayerStand_body_exited(body : PhysicsBody2D) -> void:
	if body is Player:
		sinking = false
		SoundController.play_sound("platform_rise")

func _ready() -> void:
	if randf() > 0.45:
		sprite.flip_h = true
