extends Node2D

const SpriteBody : StreamTexture = preload("res://sprites/boss_body.png")
const SpriteBodySmall : StreamTexture = preload("res://sprites/boss_body_small.png")
const SpriteBodySmaller : StreamTexture = preload("res://sprites/boss_body_smaller.png")
const SpriteShoulder : StreamTexture = preload("res://sprites/boss_shoulder.png")
const SpriteArm : StreamTexture = preload("res://sprites/boss_arm.png")

onready var head : Sprite = $Sprite_Head
onready var mouth : Sprite = $Sprite_Head/Sprite_Mouth
onready var position_left_shoulder : Position2D = $Sprite_Head/Position2D_LeftShoulder
onready var position_right_shoulder : Position2D = $Sprite_Head/Position2D_RightShoulder
onready var position_left_arm : Position2D = $Position2D_LeftArm
onready var position_right_arm : Position2D = $Position2D_RightArm

onready var asdf : float = 0.0

func draw_sprite_at_interval(sprite : StreamTexture, start_pos : Vector2, end_pos : Vector2, i : float, raise : float = 0.0, modulate : Color = Color.white) -> void:
	var pos : Vector2 = lerp(start_pos, end_pos, i)
	if pos.y > start_pos.y:
		pos.y = lerp(start_pos.y, end_pos.y, pow(i, 2.5)) - raise
	else:
		pos.y = lerp(start_pos.y, end_pos.y, pow(i, 0.75)) - raise
	draw_texture(sprite, pos-(sprite.get_size() / 2.0), modulate)

func draw_arm(shoulder_pos : Vector2, arm_pos : Vector2) -> void:
	draw_sprite_at_interval(SpriteArm, shoulder_pos, arm_pos, 0.2)
	draw_sprite_at_interval(SpriteArm, shoulder_pos, arm_pos, 0.4)
	draw_sprite_at_interval(SpriteArm, shoulder_pos, arm_pos, 0.6)
	draw_sprite_at_interval(SpriteArm, shoulder_pos, arm_pos, 0.8)
	draw_sprite_at_interval(SpriteArm, shoulder_pos, arm_pos, 1.0)

func _draw() -> void:
	# Draw body
	draw_texture(SpriteBodySmall, -(SpriteBodySmall.get_size() / 2.0), Color(0.15, 0.15, 0.15))
	draw_sprite_at_interval(SpriteBodySmaller, Vector2.ZERO, head.position + Vector2(0, 48), 0.1, 8.0, Color(0.3, 0.3, 0.3))
	draw_sprite_at_interval(SpriteBodySmaller, Vector2.ZERO, head.position + Vector2(0, 48), 0.25, 16.0, Color(0.45, 0.45, 0.45))
	draw_sprite_at_interval(SpriteBodySmaller, Vector2.ZERO, head.position + Vector2(0, 48), 0.4, 28.0, Color(0.6, 0.6, 0.6))
	draw_sprite_at_interval(SpriteBodySmall, Vector2.ZERO, head.position + Vector2(0, 48), 0.6, 32.0, Color(0.75, 0.75, 0.75))
	draw_sprite_at_interval(SpriteBody, Vector2.ZERO, head.position + Vector2(0, 48), 0.85, 24.0, Color(0.9, 0.9, 0.9))
	# Draw arms
	draw_arm(head.position + position_left_shoulder.position, position_left_arm.position)
	draw_arm(head.position + position_right_shoulder.position, position_right_arm.position)
	draw_texture(SpriteShoulder, head.position + position_left_shoulder.position-(SpriteShoulder.get_size() / 2.0))
	draw_texture(SpriteShoulder, head.position + position_right_shoulder.position-(SpriteShoulder.get_size() / 2.0))

func _process(delta : float) -> void:
	asdf += delta / 2.0
	head.position = Vector2(sin(asdf) * 128.0, -96.0 + (sin(asdf * 2.0) * 64.0))
	mouth.position = Vector2(0.0, 16.0 + cos(asdf * 2.0) * 16.0)
	update()
