extends Sprite

const _TurretShot : PackedScene = preload("res://objects/game/TurretShot.tscn")
const _Particle : PackedScene = preload("res://objects/particles/Particle.tscn")

const SHOT_SPEED : float = 128.0

const GUN_DIRECTIONS : Dictionary = {
	Vector2.LEFT: {"rotation": 0.0, "flip_h": true, "flip_v": false, "frame": 0},
	Vector2.RIGHT: {"rotation": 0.0, "flip_h": false, "flip_v": false, "frame": 0},
	Vector2.UP: {"rotation": 90.0, "flip_h": true, "flip_v": true, "frame": 0},
	Vector2.DOWN: {"rotation": 90.0, "flip_h": false, "flip_v": true, "frame": 0},
	Vector2(1, 1): {"rotation": 0.0, "flip_h": false, "flip_v": false, "frame": 1},
	Vector2(-1, 1): {"rotation": 0.0, "flip_h": true, "flip_v": false, "frame": 1},
	Vector2(-1, -1): {"rotation": -90.0, "flip_h": false, "flip_v": true, "frame": 1},
	Vector2(1, -1): {"rotation": -90.0, "flip_h": false, "flip_v": false, "frame": 1}
}

onready var sprite_gun : Sprite = $Sprite_Gun
onready var timer_fire : Timer = $Timer_Fire

var firing_direction : Vector2

func set_firing_direction(direction : Vector2) -> void:
	firing_direction = direction
	var direction_properties : Dictionary = GUN_DIRECTIONS[direction]
	sprite_gun.rotation_degrees = direction_properties["rotation"]
	sprite_gun.flip_h = direction_properties["flip_h"]
	sprite_gun.flip_v = direction_properties["flip_v"]
	sprite_gun.frame = direction_properties["frame"]

func set_firing_interval(interval : float) -> void:
	timer_fire.wait_time = interval
	timer_fire.start()

func _process(delta : float) -> void:
	sprite_gun.position = lerp(sprite_gun.position, Vector2.ZERO, delta * 5.0)

func _on_Timer_Fire_timeout():
	var shot : Node2D = _TurretShot.instance()
	shot.global_position = global_position + (firing_direction.clamped(1.0) * 8.0)
	shot.velocity = firing_direction * SHOT_SPEED
	get_parent().add_child(shot)
	# Visual flair
	sprite_gun.position = firing_direction * -2.0
	var particle : Sprite = _Particle.instance()
	particle.global_position = global_position + (firing_direction.clamped(1.0) * 14.0)
	particle.velocity = firing_direction * 32.0
	get_parent().add_child(particle)
	particle.set_particle_type(particle.TYPE.GUNFLASH)
	SoundController.play_sound("turret_shoot")
