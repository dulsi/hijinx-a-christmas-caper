extends Node

const _Player : PackedScene = preload("res://objects/game/Player.tscn")
const _GoldenRing : PackedScene = preload("res://objects/game/GoldenRing.tscn")
const _Goal : PackedScene = preload("res://objects/game/Goal.tscn")
const _Spike : PackedScene = preload("res://objects/game/Spike.tscn")
const _MovingPlatform : PackedScene = preload("res://objects/game/MovingPlatform.tscn")
const _SinkingPlatform : PackedScene = preload("res://objects/game/SinkingPlatform.tscn")
const _Spark : PackedScene = preload("res://objects/game/Spark.tscn")
const _Crumbler : PackedScene = preload("res://objects/game/Crumbler.tscn")
const _BowlingBall : PackedScene = preload("res://objects/game/BowlingBall.tscn")
const _Springboard : PackedScene = preload("res://objects/game/Springboard.tscn")
const _EnemyBlocker : PackedScene = preload("res://objects/game/EnemyBlocker.tscn")
const _Stalactite : PackedScene = preload("res://objects/game/Stalactite.tscn")
const _Orb : PackedScene = preload("res://objects/game/Orb.tscn")
const _OrbController : PackedScene = preload("res://objects/game/OrbController.tscn")
const _Hammer : PackedScene = preload("res://objects/game/Hammer.tscn")
const _SpringboardPlatform : PackedScene = preload("res://objects/game/SpringboardPlatform.tscn")
const _Turret : PackedScene = preload("res://objects/game/Turret.tscn")
const _Carl : PackedScene = preload("res://objects/game/Carl.tscn")

const PLAYER_OFFSET : Vector2 = Vector2(8, -7)

onready var dirt : TileMap = $Dirt
onready var ice : TileMap = $Ice
onready var brick : TileMap = $Brick
onready var snowtop : TileMap = $Snowtop
onready var snowover : TileMap = $Snowover
onready var snowbottom : TileMap = $Snowbottom

var game : Node
var player : Node2D
var goal : Node2D

func add_dirt_tile(pos : Vector2) -> void:
	dirt.set_cellv(pos, 0)

func add_ice_tile(pos : Vector2) -> void:
	ice.set_cellv(pos, 1)

func add_brick_tile(pos : Vector2) -> void:
	brick.set_cellv(pos, 2)

func add_player(pos : Vector2) -> void:
	var player : Node2D = _Player.instance()
	player.position = (pos * Constants.TILE_SIZE) + PLAYER_OFFSET
	add_child(player)
	player.connect("player_dead", game, "player_dead")
	self.player = player

func add_object(pos : Vector2, which_object : String) -> void:
	match which_object:
		Constants.EDITOR_ITEM_NAME_RING:
			var ring : Node2D = _GoldenRing.instance()
			ring.position = (pos * Constants.TILE_SIZE) + Vector2(8, 8)
			ring.connect("ring_collected", game, "ring_collected")
			add_child(ring)
		Constants.EDITOR_ITEM_NAME_GOAL:
			var goal : Node2D = _Goal.instance()
			goal.position = (pos * Constants.TILE_SIZE) + Vector2(8, 8)
			add_child(goal)
			goal.connect("goal_reached", game, "level_cleared")
			self.goal = goal
		Constants.EDITOR_ITEM_NAME_SINKING_PLATFORM:
			var platform : Node2D = _SinkingPlatform.instance()
			platform.position = (pos * Constants.TILE_SIZE) + Vector2(8, 0)
			add_child(platform)
		Constants.EDITOR_ITEM_NAME_CRUMBLER:
			var crumbler : Node2D = _Crumbler.instance()
			crumbler.position = pos * Constants.TILE_SIZE
			add_child(crumbler)
		Constants.EDITOR_ITEM_NAME_BOUNCING_BALL:
			var ball : Node2D = _BowlingBall.instance()
			ball.position = pos * Constants.TILE_SIZE
			add_child(ball)
		Constants.EDITOR_ITEM_NAME_SPRINGBOARD:
			var springboard : Node2D = _Springboard.instance()
			springboard.position = (pos * Constants.TILE_SIZE) + Vector2(8, 16)
			add_child(springboard)
		Constants.EDITOR_ITEM_NAME_ENEMY_BLOCKER:
			var blocker : Node2D = _EnemyBlocker.instance()
			blocker.position = pos * Constants.TILE_SIZE
			add_child(blocker)
		Constants.EDITOR_ITEM_NAME_STALACTITE:
			var stalactite : Node2D = _Stalactite.instance()
			stalactite.position = (pos * Constants.TILE_SIZE) + Vector2(8, 0)
			add_child(stalactite)
		Constants.EDITOR_ITEM_NAME_TURRET:
			var turret : Node2D = _Turret.instance()
			turret.position = pos * Constants.TILE_SIZE
			add_child(turret)

func add_spike(pos : Vector2, frame : int) -> void:
	var spike : Node2D = _Spike.instance()
	spike.position = (pos * Constants.TILE_SIZE) + Vector2(8, 8)
	spike.frame = frame
	add_child(spike)
	spike.update_hitbox()

func add_spark(pos : Vector2, anticlockwise : bool) -> void:
	var spark : Node2D = _Spark.instance()
	spark.position = pos * Constants.TILE_SIZE
	spark.moving_anticlockwise = anticlockwise
	add_child(spark)

func add_moving_platform(pos : Vector2, initial_direction : Vector2) -> void:
	var platform : Node2D = _MovingPlatform.instance()
	platform.position = (pos * Constants.TILE_SIZE) + Vector2(8, 0)
	platform.direction = initial_direction
	add_child(platform)

func add_orb(pos : Vector2, colour : int) -> void:
	var orb : Node2D = _Orb.instance()
	orb.position = (pos * Constants.TILE_SIZE) + Vector2(8, 8)
	orb.which_colour = colour
	add_child(orb)
	orb.set_colour(colour)

func add_hammer(pos : Vector2, starting_delay : float) -> void:
	var hammer : Node2D = _Hammer.instance()
	hammer.position = (pos * Constants.TILE_SIZE) + Vector2(8, 16)
	hammer.grounded_wait_time = starting_delay
	add_child(hammer)

func add_orb_controller() -> void:
	var controller : Node = _OrbController.instance()
	add_child(controller)
	controller.goal = goal
	goal.make_hidden()

func add_springboard_platform(pos : Vector2, initial_direction : Vector2) -> void:
	var platform : Node2D = _SpringboardPlatform.instance()
	platform.position = (pos * Constants.TILE_SIZE) + Vector2(8, 0)
	platform.direction = initial_direction
	add_child(platform)

func add_turret(pos : Vector2, firing_direction : Vector2, firing_interval : float) -> void:
	var turret : Node2D = _Turret.instance()
	turret.position = (pos * Constants.TILE_SIZE) + Vector2(8, 8)
	add_child(turret)
	turret.set_firing_direction(firing_direction)
	turret.set_firing_interval(firing_interval)

func add_carl(pos : Vector2, which_conversation : String) -> void:
	var carl : Node2D = _Carl.instance()
	carl.position = (pos * Constants.TILE_SIZE) + Vector2(8, 16)
	carl.which_conversation = which_conversation
	add_child(carl)
	carl.connect("conversation_started", game, "start_conversation")

func get_tile_type_at(pos : Vector2) -> int:
	if ice.get_cellv(pos) != -1:
		return Constants.LEVELTILE_ICE
	elif brick.get_cellv(pos) != -1:
		return Constants.LEVELTILE_BRICK
	elif dirt.get_cellv(pos) != -1:
		return Constants.LEVELTILE_DIRT
	else:
		return Constants.LEVELTILE_EMPTY

# Returns the maximum width of all three tilemaps
func get_map_size() -> Vector2:
	var result = Rect2(0, 0, 1, 1)
	result = result.merge(dirt.get_used_rect())
	result = result.merge(ice.get_used_rect())
	result = result.merge(brick.get_used_rect())
	return result.size

func update_bitmasks() -> void:
	dirt.update_bitmask_region()
	ice.update_bitmask_region()
	brick.update_bitmask_region()

func update_snow_tiles() -> void:
	# Find all brick tiles
	for pos in brick.get_used_cells():
		# If there's dirt below this tile, add some snow around the base
		if get_tile_type_at(pos + Vector2.DOWN) == Constants.LEVELTILE_DIRT:
			snowbottom.set_cellv(pos, 4)
		# If there's nothing above this tile, add some snow to it
		snowtop.set_cellv(pos, 3)
		# Let's get ready for some JAAAAANNNNNNK!
		var cursor : Vector2 = pos + Vector2.UP
		while get_tile_type_at(cursor) in [Constants.LEVELTILE_DIRT, Constants.LEVELTILE_ICE]:
			snowtop.set_cellv(cursor, 3)
			cursor += Vector2.UP
		# If there's air above this tile, add some nice snow
		if get_tile_type_at(pos + Vector2.UP) == Constants.LEVELTILE_EMPTY:
			if randf() > 0.5:
				snowover.set_cellv(pos + Vector2.UP, 5)
	# Find all dirt tiles
	for pos in dirt.get_used_cells():
		# If there's air above this tile, add some nice snow
		if get_tile_type_at(pos + Vector2.UP) == Constants.LEVELTILE_EMPTY:
			snowover.set_cellv(pos + Vector2.UP, 5)
	snowtop.update_bitmask_region()
	snowover.update_bitmask_region()
	snowbottom.update_bitmask_region()
