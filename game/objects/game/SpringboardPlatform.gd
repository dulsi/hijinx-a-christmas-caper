extends KinematicBody2D

class_name SpringboardPlatform

const MOVE_SPEED : float = 64.0

onready var sprite : Sprite = $Sprite
onready var sprite_thruster : Sprite = $Sprite/Sprite_Thruster

var anim_index : float = 0
var direction : Vector2

func _physics_process(delta : float) -> void:
	anim_index += delta
	sprite_thruster.frame = wrapi(anim_index * 10.0, 0, sprite_thruster.hframes)
	# Try to move
	var movement_amount : Vector2 = direction * MOVE_SPEED * delta
	var original_y : float = position.y
	var collision : KinematicCollision2D = move_and_collide(movement_amount)
	position.y = original_y
	if collision != null:
		if collision.collider is Player or collision.collider is Stalactite:
			# Tell the player to move or die (ayy namedrop!)
			collision.collider.move_or_be_crushed(collision.remainder, self)
			# Then try again
			position += collision.remainder
		else:
			direction *= -1 # Turn around
