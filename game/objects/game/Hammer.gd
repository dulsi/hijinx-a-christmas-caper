extends KinematicBody2D

const RISING_SPEED : float = 32.0
const DISTANCE_TO_RISE : float = 80.0
const MAX_FALL_SPEED : float = 512.0
const FALL_INCR : float = 512.0
const WAIT_TIME : float = 2.00

enum STATE {GROUNDED, RISING, FALLING}

var current_state : int = STATE.GROUNDED
var velocity : Vector2 = Vector2.ZERO
var grounded_wait_time : float
var distance_risen : float

func grounded(delta : float) -> void:
	# Make sure we still have ground beneath us
	if test_move(transform, Vector2.DOWN) == false:
		current_state = STATE.FALLING
	else:
		# Wait to rise
		grounded_wait_time -= delta
		if grounded_wait_time <= 0.0:
			current_state = STATE.RISING
			SoundController.play_sound("hammer_rise")

func rising(delta : float) -> void:
	var rising_amount : float = RISING_SPEED * delta
	var original_x : float = position.x
	var collision : KinematicCollision2D = move_and_collide(Vector2.UP * rising_amount)
	# Don't let the player move us horizontally
	position.x = original_x
	if collision != null:
		if collision.collider is Player:
			collision.collider.move_or_be_crushed(Vector2.UP * rising_amount, self)
		if collision.normal == Vector2.UP:
			# Honestly, bollocks to it, let's just rise up
			position.y += collision.remainder.y
	distance_risen += rising_amount
	if distance_risen > DISTANCE_TO_RISE:
		current_state = STATE.FALLING
		velocity.y = 0.0
		distance_risen = 0.0

func falling(delta : float) -> void:
	velocity.y = clamp(velocity.y + (FALL_INCR * delta), -MAX_FALL_SPEED, MAX_FALL_SPEED)
	var collision : KinematicCollision2D = move_and_collide(velocity * delta)
	if collision != null:
		if collision.collider is Player:
			collision.collider.get_crushed()
		else:
			current_state = STATE.GROUNDED
			if velocity.y > 128.0:
				SoundController.play_sound("bowling_ball_bounce")
			velocity.y = 0.0
			grounded_wait_time = WAIT_TIME

func _physics_process(delta : float) -> void:
	match current_state:
		STATE.GROUNDED:
			grounded(delta)
		STATE.RISING:
			rising(delta)
		STATE.FALLING:
			falling(delta)
