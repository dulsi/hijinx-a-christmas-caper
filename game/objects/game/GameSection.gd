extends Node2D

const RoomLoader : GDScript = preload("res://scripts/RoomLoader.gd")

onready var geometry : Node = $Geometry
onready var background : Node = $Background
var level_data : Dictionary

onready var rings_collected : int = 0
onready var start_time : int
onready var time_spent_paused : int = 0
var pause_start_time : int

signal game_over
signal section_cleared
signal conversation_started

func start_timer() -> void:
	start_time = OS.get_ticks_msec()

func get_clear_time() -> int:
	return OS.get_ticks_msec() - start_time - time_spent_paused

func pause_timer() -> void:
	pause_start_time = OS.get_ticks_msec()

func resume_timer() -> void:
	time_spent_paused += OS.get_ticks_msec() - pause_start_time

func ring_collected() -> void:
	rings_collected += 1

func start_conversation(conversation_name : String) -> void:
	emit_signal("conversation_started", conversation_name)

func player_dead() -> void:
	emit_signal("game_over")

func level_cleared() -> void:
	emit_signal("section_cleared")

func set_bg_palette(palette : int) -> void:
	background.set_palette(palette)

func set_bg_mountains(which : int) -> void:
	background.set_mountains(which)

func _ready() -> void:
	geometry.game = self
	RoomLoader.load_room(level_data, geometry)
