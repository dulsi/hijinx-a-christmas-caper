extends Node2D

const _Particle : PackedScene = preload("res://objects/particles/Particle.tscn")

onready var colour : int = 0
onready var sparkle_angle : float = 0.0
onready var target_position : Vector2 = global_position
onready var velocity : Vector2 = Vector2.ZERO

func make_sparkle() -> void:
	for dir in [Vector2.LEFT, Vector2.RIGHT]:
		# EXECUTE FABULOUSITY PROTOCOL
		var particle : Sprite = _Particle.instance()
		particle.global_position = global_position + (dir.rotated(sparkle_angle) * 12.0)
		particle.velocity = velocity
		get_parent().add_child(particle)
		particle.no_accel = true
		match colour:
			0: particle.set_particle_type(particle.TYPE.RED_SPARKLE)
			1: particle.set_particle_type(particle.TYPE.YELLOW_SPARKLE)
			2: particle.set_particle_type(particle.TYPE.GREEN_SPARKLE)
			3: particle.set_particle_type(particle.TYPE.BLUE_SPARKLE)

func set_target_position(pos : Vector2, col : int) -> void:
	target_position = pos
	colour = col

func _process(delta : float) -> void:
	sparkle_angle += delta * 8.0
	var old_global_position : Vector2 = global_position
	global_position = lerp(global_position, target_position, delta * 5.0)
	var diff : Vector2 = global_position - old_global_position
	velocity = diff * 10.0

func _on_Timer_EmitSparkle_timeout() -> void:
	make_sparkle()
