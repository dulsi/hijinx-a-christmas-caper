extends Area2D

onready var sprite : Sprite = $Sprite

var velocity : Vector2
onready var anim_index : float = 0.0

func _process(delta : float) -> void:
	position += velocity * delta
	anim_index += delta
	sprite.frame = wrapi(anim_index * 15.0, 0, sprite.hframes)

func _on_TurretShot_body_entered(body : PhysicsBody2D) -> void:
	if body is Player:
		body.get_hurt_by_shot()
