extends StaticBody2D

const _Particle : PackedScene = preload("res://objects/particles/Particle.tscn")

class_name Crumbler

onready var sprite : Sprite = $Sprite
onready var area_player_stand : Area2D = $Area2D_PlayerStand
onready var timer_crumble : Timer = $Timer_Crumble

onready var crumbling : bool = false
onready var shake_amount : float = 0.0

func _process(delta : float) -> void:
	if crumbling:
		sprite.offset.y = randf() * 2.0 # shake it, baby

func _on_Area2D_PlayerStand_body_entered(body : PhysicsBody2D) -> void:
	# If we're not pre-crumbled, this doesn't matter
	if crumbling:
		return
	# Did the player just step on us?
	if body is Player:
		crumbling = true
		timer_crumble.start()
		SoundController.play_sound("crumbler_crack")
		shake_amount = 1.0
		for i in range(0, 8):
			var particle : Sprite = _Particle.instance()
			particle.global_position = global_position + (Vector2(randf(), randf()) * 16.0)
			particle.velocity = Vector2.DOWN * randf() * 64.0
			get_parent().add_child(particle)
			particle.set_particle_type(particle.TYPE.DUST)

func _on_Timer_Crumble_timeout() -> void:
	SoundController.play_sound("crumbler_break")
	for direction in [Vector2(-1, -1), Vector2(1, -1), Vector2(-1, 1), Vector2(1, 1)]:
		var particle : Sprite = _Particle.instance()
		particle.global_position = global_position + Vector2(8, 8) + (direction * 4)
		particle.velocity = direction * 64.0
		get_parent().add_child(particle)
		particle.set_particle_type(particle.TYPE.BRICK)
	queue_free()
