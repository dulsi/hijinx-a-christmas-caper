extends Sprite

const _Particle : PackedScene = preload("res://objects/particles/Particle.tscn")

onready var tween : Tween = $Tween

var colour : int

onready var spin_index : float = 0.0
onready var radius_coefficient : float = 0.0

func move_on_target(target_position : Vector2) -> void:
	tween.interpolate_property(self, "position", position, target_position, 0.5)
	tween.interpolate_property(self, "radius_coefficient", 1.0, 0.0, 0.5)
	tween.start()
	yield(tween, "tween_all_completed")
	queue_free()

func _process(delta : float) -> void:
	spin_index += delta * 2.0
	offset = Vector2(sin(spin_index) * 16.0, sin(spin_index * 2.0) * 8.0) * radius_coefficient

func _on_Timer_NextFrame_timeout() -> void:
	var first_frame : int = colour * 8
	if frame >= first_frame + 7:
		frame = first_frame
	else:
		frame += 1
	# EXECUTE FABULOUSITY PROTOCOL
	var particle : Sprite = _Particle.instance()
	particle.global_position = global_position + offset + (Vector2(randf() - 0.5, randf() - 0.5) * 12.0)
	particle.velocity = Vector2.DOWN * randf() * 64.0
	get_parent().add_child(particle)
	match colour:
		0: particle.set_particle_type(particle.TYPE.RED_SPARKLE)
		1: particle.set_particle_type(particle.TYPE.YELLOW_SPARKLE)
		2: particle.set_particle_type(particle.TYPE.GREEN_SPARKLE)
		3: particle.set_particle_type(particle.TYPE.BLUE_SPARKLE)

func _ready() -> void:
	frame = colour * 8
	tween.interpolate_property(self, "position", position, position - Vector2(0, 32), 0.5, Tween.TRANS_SINE, Tween.EASE_OUT)
	tween.interpolate_property(self, "radius_coefficient", 0.0, 1.0, 2.0, Tween.TRANS_SINE, Tween.EASE_OUT)
	tween.start()
