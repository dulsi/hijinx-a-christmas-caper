extends Area2D

onready var sprite : Sprite = $Sprite
onready var collision_omni : CollisionPolygon2D = $CollisionPolygon2D_Omni
onready var collision_pointed : CollisionPolygon2D = $CollisionPolygon2D_Pointed

var frame : int

func update_hitbox() -> void:
	match sprite.frame:
		0:
			collision_omni.disabled = false
			collision_pointed.disabled = true
		1:
			collision_omni.disabled = true
			collision_pointed.disabled = false
			collision_pointed.rotation_degrees = 0.0
		2:
			collision_omni.disabled = true
			collision_pointed.disabled = false
			collision_pointed.rotation_degrees = 180.0
		3:
			collision_omni.disabled = true
			collision_pointed.disabled = false
			collision_pointed.rotation_degrees = 270.0
		4:
			collision_omni.disabled = true
			collision_pointed.disabled = false
			collision_pointed.rotation_degrees = 90.0

func _body_entered(body : Node) -> void:
	if body is Player:
		body.get_hurt_by_spikes()

func _ready() -> void:
	sprite.frame = frame
