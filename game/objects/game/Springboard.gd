extends StaticBody2D

onready var sprite : Sprite = $Sprite
onready var sprite_head : Sprite = $Sprite_Head
onready var tween : Tween = $Tween

func do_tween() -> void:
	tween.interpolate_property(sprite_head, "position", Vector2(-8, -11), Vector2(-8, -22), 0.125, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	tween.interpolate_property(sprite_head, "region_rect", Rect2(0, 0, 16, 10), Rect2(0, 0, 16, 20), 0.125, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	tween.interpolate_property(sprite_head, "position", Vector2(-8, -22), Vector2(-8, -11), 0.5, Tween.TRANS_BACK, Tween.EASE_OUT, 0.125)
	tween.interpolate_property(sprite_head, "region_rect", Rect2(0, 0, 16, 20), Rect2(0, 0, 16, 10), 0.5, Tween.TRANS_BACK, Tween.EASE_OUT, 0.125)
	tween.start()

func _on_Area2D_PlayerStand_body_entered(body : PhysicsBody2D) -> void:
	if body is Player:
		body.get_sprung()
		do_tween()
		SoundController.play_sound("springboard_bounce")
