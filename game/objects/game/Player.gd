extends KinematicBody2D

class_name Player

const _Particle : PackedScene = preload("res://objects/particles/Particle.tscn")

const RUN_SPEED : float = 128.0
const RUN_INCR : float = 2048.0
const RUN_DRAG : float = 1024.0
const ON_ICE_RUN_INCR : float = 256.0
const ON_ICE_RUN_DRAG : float = 32.0
const ANIM_SPEED : float = 10.0
const JUMP_SPEED : float = 256.0
const SPRING_JUMP_SPEED : float = 384.0
const MAX_FALL_SPEED : float = 512.0
const FALL_INCR : float = 512.0
const WALL_DESCEND_SPEED : float = 64.0
const WALL_JUMP_SPEED : float = 64.0
const COYOTE_TIME_AMOUNT : float = 0.1
const WALL_JUMP_TIME_AMOUNT : float = 0.1
const KNOCKBACK_VELOCITY : Vector2 = Vector2(-64.0, -96.0)
const WALL_DUST_INTERVAL : float = 0.15

onready var sprite : Sprite = $Sprite
onready var collision_shape : CollisionShape2D = $CollisionShape2D
onready var tween : Tween = $Tween

enum PLAYER_STATE {ON_GROUND, IN_AIR, WALL_JUMP, HURT}
enum SPRITE_ANIM {IDLE, RUN, JUMP, FALL, LAND, WALL_GRAB, WALL_JUMP, HURT}

onready var current_state : int = PLAYER_STATE.ON_GROUND
onready var current_anim : int = SPRITE_ANIM.IDLE
onready var anim_index : float = 0.0
onready var velocity : Vector2 = Vector2.ZERO
onready var wall_jump_time : float = 0.0
onready var coyote_time : float = 0.0
onready var spring_jump : bool = false
onready var wall_dust_cooldown : float = 0.0

signal player_dead

func do_tween_jump() -> void:
	sprite.position.y = 0
	sprite.offset.y = 0
	tween.interpolate_property(sprite, "scale", Vector2(0.75, 1.25), Vector2.ONE, 0.25)
	tween.start()

func do_tween_land() -> void:
	sprite.position.y = 24
	sprite.offset.y = -24
	tween.interpolate_property(sprite, "scale", Vector2(1.25, 0.75), Vector2.ONE, 0.25)
	tween.start()

func do_tween_hurt() -> void:
	sprite.position.y = 0
	sprite.offset.y = 0
	tween.interpolate_property(sprite, "scale", Vector2(0.625, 1.5), Vector2.ONE, 0.5, Tween.TRANS_ELASTIC, Tween.EASE_OUT)
	tween.start()

func emit_run_particle() -> void:
	var direction : Vector2 = Vector2.RIGHT if sprite.flip_h else Vector2.LEFT
	var particle : Sprite = _Particle.instance()
	particle.global_position = global_position + Vector2(8 * direction.x, 18)
	particle.velocity = direction * 32.0
	particle.flip_h = sprite.flip_h
	get_parent().add_child(particle)
	particle.set_particle_type(particle.TYPE.RUN_DUST)

func emit_wall_particle() -> void:
	var direction : Vector2 = Vector2.RIGHT if sprite.flip_h else Vector2.LEFT
	var particle : Sprite = _Particle.instance()
	particle.global_position = global_position + Vector2(-12 * direction.x, 18)
	particle.velocity = Vector2(direction.x * -8.0, -8)
	particle.flip_h = sprite.flip_h
	get_parent().add_child(particle)
	particle.set_particle_type(particle.TYPE.SLIDE_DUST)

func emit_land_particle() -> void:
	for direction in [Vector2.LEFT, Vector2.RIGHT]:
		var particle : Sprite = _Particle.instance()
		particle.global_position = global_position + Vector2(8 * direction.x, 20)
		particle.velocity = direction * 32.0
		get_parent().add_child(particle)
		particle.set_particle_type(particle.TYPE.LAND_DUST)

func update_sprite_frame() -> void:
	match current_anim:
		SPRITE_ANIM.IDLE:
			if wrapi(anim_index, 0, 90) > 84:
				sprite.frame = 5 - wrapi(anim_index, 0, 5)
			elif wrapi(anim_index, 0, 90) > 59:
				sprite.frame = 5
			elif wrapi(anim_index, 0, 90) > 55:
				sprite.frame = wrapi(anim_index, 0, 5)
			else:
				sprite.frame = 0
		SPRITE_ANIM.RUN:
			sprite.frame = 6 + wrapi(anim_index, 0, 8)
			if sprite.frame in [6, 10]:
				emit_run_particle()
			elif sprite.frame in [9, 13]:
				# Make some noise!
				match get_ground_type():
					Constants.LEVELTILE_BRICK:
						SoundController.play_sound("footstep_brick")
					Constants.LEVELTILE_DIRT:
						SoundController.play_sound("footstep_dirt")
					Constants.LEVELTILE_ICE:
						SoundController.play_sound("footstep_ice")
					Constants.EDITOR_ITEM_ID_HAMMER:
						SoundController.play_sound("footstep_metal")
		SPRITE_ANIM.JUMP:
			sprite.frame = 15 + clamp(anim_index, 0, 1)
		SPRITE_ANIM.FALL:
			sprite.frame = 17 + clamp(anim_index, 0, 1)
		SPRITE_ANIM.LAND:
			if anim_index < 7:
				sprite.frame = 19 + anim_index
			else:
				current_anim = SPRITE_ANIM.IDLE
				anim_index = 0.0
		SPRITE_ANIM.WALL_GRAB:
			sprite.frame = 26 + clamp(anim_index, 0, 2)
		SPRITE_ANIM.WALL_JUMP:
			sprite.frame = 29 + clamp(anim_index, 0, 2)
		SPRITE_ANIM.HURT:
			sprite.frame = 32 + clamp(anim_index, 0, 2)

# Weird name, just to make sure there's no confusion between this and `is_on_ground()`.
func is_grounded() -> bool:
	var collision : KinematicCollision2D = move_and_collide(Vector2.DOWN, true, true, true)
	return collision != null

func check_for_slippy(direction : Vector2) -> bool:
	var collision : KinematicCollision2D = move_and_collide(direction, true, true, true)
	if collision != null:
		return collision.collider.is_in_group("slippery")
	return false

func get_ground_type() -> int:
	var collision : KinematicCollision2D = move_and_collide(Vector2.DOWN, true, true, true)
	if collision != null:
		if collision.collider.is_in_group("brick"):
			return Constants.LEVELTILE_BRICK
		elif collision.collider.is_in_group("dirt"):
			return Constants.LEVELTILE_DIRT
		elif collision.collider.is_in_group("ice"):
			return Constants.LEVELTILE_ICE
		# janky hack, m8!
		elif collision.collider.is_in_group("hammer"):
			return Constants.EDITOR_ITEM_ID_HAMMER
		elif collision.collider.is_in_group("platform"):
			return Constants.EDITOR_ITEM_ID_HAMMER
	return Constants.LEVELTILE_EMPTY

func add_running_momentum(direction : Vector2, delta : float) -> void:
	var incr : float = RUN_INCR
	if check_for_slippy(Vector2.DOWN):
		incr = ON_ICE_RUN_INCR
	velocity.x = clamp(velocity.x + (direction.x * incr * delta), -RUN_SPEED, RUN_SPEED)
	sprite.flip_h = direction == Vector2.LEFT
	if current_state == PLAYER_STATE.ON_GROUND and current_anim != SPRITE_ANIM.RUN:
		current_anim = SPRITE_ANIM.RUN
		anim_index = 0.0
		update_sprite_frame()

func slow_down(delta : float) -> void:
	var drag : float = RUN_DRAG
	if check_for_slippy(Vector2.DOWN):
		drag = ON_ICE_RUN_DRAG
	# Slow down
	if velocity.x > 0:
		velocity.x = clamp(velocity.x - (drag * delta), 0, RUN_SPEED)
	elif velocity.x < 0:
		velocity.x = clamp(velocity.x + (drag * delta), -RUN_SPEED, 0)

func try_to_move(delta : float) -> void:
	var collision : KinematicCollision2D = move_and_collide(velocity * delta * Vector2(1, 0))
	if collision != null:
		if collision.normal == Vector2.LEFT:
			velocity.x = clamp(velocity.x, -RUN_SPEED, 0)
		elif collision.normal == Vector2.RIGHT:
			velocity.x = clamp(velocity.x, 0, RUN_SPEED)

func try_to_interact() -> void:
	var interactables : Array = get_tree().get_nodes_in_group("interactable")
	for interactable in interactables:
		if interactable.can_interact(self):
			interactable.interact()
			return

func do_jump() -> void:
	velocity.y = -JUMP_SPEED
	current_state = PLAYER_STATE.IN_AIR
	current_anim = SPRITE_ANIM.JUMP
	anim_index = 0.0
	update_sprite_frame()
	do_tween_jump()
	SoundController.play_sound("player_jump")

func do_wall_jump() -> void:
	velocity.y = -JUMP_SPEED
	current_state = PLAYER_STATE.WALL_JUMP
	current_anim = SPRITE_ANIM.WALL_JUMP
	anim_index = 0.0
	update_sprite_frame()
	wall_jump_time = WALL_JUMP_TIME_AMOUNT
	coyote_time = 0.0
	do_tween_jump()
	SoundController.play_sound("player_jump")

# I do not apologise for the name of this function.
func get_sprung() -> void:
	spring_jump = true
	velocity.y = -SPRING_JUMP_SPEED
	current_state = PLAYER_STATE.IN_AIR
	current_anim = SPRITE_ANIM.JUMP
	anim_index = 0.0
	update_sprite_frame()
	do_tween_jump()

func land() -> void:
	current_state = PLAYER_STATE.ON_GROUND
	current_anim = SPRITE_ANIM.LAND
	anim_index = 0.0
	update_sprite_frame()
	spring_jump = false
	if velocity.y > 200.0: # ew, magic numbers!
		do_tween_land()
	velocity.y = 0.0
	emit_land_particle()
	# Make some noise!
	match get_ground_type():
		Constants.LEVELTILE_BRICK:
			SoundController.play_sound("land_brick")
		Constants.LEVELTILE_DIRT:
			SoundController.play_sound("land_dirt")
		Constants.LEVELTILE_ICE:
			SoundController.play_sound("land_ice")
		Constants.EDITOR_ITEM_ID_HAMMER:
			SoundController.play_sound("land_metal")
	
func on_ground(delta : float) -> void:
	if Input.is_action_pressed("run_right"):
		add_running_momentum(Vector2.RIGHT, delta)
	elif Input.is_action_pressed("run_left"):
		add_running_momentum(Vector2.LEFT, delta)
	else:
		slow_down(delta)
		# If we're running, go back to idle
		if current_anim == SPRITE_ANIM.RUN:
			current_anim = SPRITE_ANIM.IDLE
			anim_index = 0.0
			update_sprite_frame()
	try_to_move(delta)
	# Make sure we aren't standing on thin air
	if not is_grounded():
		current_state = PLAYER_STATE.IN_AIR
		current_anim = SPRITE_ANIM.FALL
		anim_index = 0.0
		update_sprite_frame()
		velocity.y = 0.0
		coyote_time = COYOTE_TIME_AMOUNT
	# If we're still on the ground, we can joke
	elif Input.is_action_just_pressed("jump"):
		do_jump()
	if Input.is_action_just_pressed("interact"):
		try_to_interact()

func in_air(delta : float) -> void:
	# Move about in the air, totally like how you can in real life (try it!)
	if Input.is_action_pressed("run_right"):
		add_running_momentum(Vector2.RIGHT, delta)
	elif Input.is_action_pressed("run_left"):
		add_running_momentum(Vector2.LEFT, delta)
	else:
		slow_down(delta)
	try_to_move(delta)
	# Have we let go of jump?
	var max_upwards_speed : float = -MAX_FALL_SPEED
	if (not Input.is_action_pressed("jump")) and not spring_jump:
		max_upwards_speed /= 4.0
	# Start falling
	velocity.y = clamp(velocity.y + FALL_INCR * delta, max_upwards_speed, MAX_FALL_SPEED)
	# Are we falling?
	var pressing_wall : bool = false
	if velocity.y > 0:
		# Change anim if needed
		if current_anim in [SPRITE_ANIM.JUMP, SPRITE_ANIM.WALL_JUMP]:
			current_anim = SPRITE_ANIM.FALL
			anim_index = 0.0
			update_sprite_frame()
		# Do we need to land?
		if is_grounded():
			land()
		# Are we up against a wall?
		if Input.is_action_pressed("run_right") and test_move(transform, Vector2.RIGHT) and not check_for_slippy(Vector2.RIGHT):
			pressing_wall = true
		if Input.is_action_pressed("run_left") and test_move(transform, Vector2.LEFT) and not check_for_slippy(Vector2.LEFT):
			pressing_wall = true
	# Do we need to get cosy?
	if pressing_wall:
		spring_jump = false
		coyote_time = COYOTE_TIME_AMOUNT # I didn't actually want to do this, but getting a good jump off a wall without is pretty hard, as it turns out
		velocity.y = clamp(velocity.y, 0, WALL_DESCEND_SPEED)
		if current_anim != SPRITE_ANIM.WALL_GRAB:
			current_anim = SPRITE_ANIM.WALL_GRAB
			anim_index = 0.0
			update_sprite_frame()
		# Wall jump?
		if Input.is_action_just_pressed("jump"):
			do_wall_jump()
		# Particles
		wall_dust_cooldown -= delta
		if wall_dust_cooldown <= 0.0:
			emit_wall_particle()
			wall_dust_cooldown = WALL_DUST_INTERVAL
	else:
		if current_anim == SPRITE_ANIM.WALL_GRAB:
			current_anim = SPRITE_ANIM.FALL
			anim_index = 0.0
			update_sprite_frame()
			coyote_time = COYOTE_TIME_AMOUNT
		# Now that we've ruled out a wall-jump, did the player hit jump just a bit too late?
		if Input.is_action_just_pressed("jump") and coyote_time > 0.0:
			do_jump()
	coyote_time = clamp(coyote_time - delta, 0.0, COYOTE_TIME_AMOUNT)
	# Move it
	var collision : KinematicCollision2D = move_and_collide(Vector2.DOWN * velocity.y * delta)
	# Did we hit something?
	if collision != null:
		# Did we hit the ceiling?
		if collision.normal == Vector2.DOWN:
			velocity.y = 0.0
	if Input.is_action_just_pressed("interact"):
		try_to_interact()

func wall_jump(delta : float) -> void:
	var away_direction : float = 1.0 if sprite.flip_h else -1.0
	var jump_velocity : Vector2 = Vector2(away_direction * WALL_JUMP_SPEED, -JUMP_SPEED)
	velocity.y = clamp(velocity.y + FALL_INCR * delta, -MAX_FALL_SPEED, MAX_FALL_SPEED)
	move_and_collide(jump_velocity * delta)
	wall_jump_time -= delta
	if wall_jump_time <= 0.0:
		current_state = PLAYER_STATE.IN_AIR

func hurt(delta : float) -> void:
	velocity.y = clamp(velocity.y + FALL_INCR * delta, -MAX_FALL_SPEED, MAX_FALL_SPEED)
	position += velocity * delta

func get_hurt() -> void:
	if current_state == PLAYER_STATE.HURT:
		return
	current_state = PLAYER_STATE.HURT
	current_anim = SPRITE_ANIM.HURT
	anim_index = 0.0
	update_sprite_frame()
	collision_shape.set_deferred("disabled", true)
	velocity = KNOCKBACK_VELOCITY
	if sprite.flip_h:
		velocity.x *= -1.0
	z_index = 250
	do_tween_hurt()
	emit_signal("player_dead")

func get_hurt_by_spikes() -> void:
	SoundController.play_sound("player_hurt")
	get_hurt()

func get_hurt_by_spark() -> void:
	SoundController.play_sound("spark_zap")
	get_hurt()

func get_hurt_by_stalactite() -> void:
	SoundController.play_sound("player_hurt")
	get_hurt()

func get_crushed() -> void:
	SoundController.play_sound("player_hurt")
	get_hurt()

func get_hurt_by_shot() -> void:
	SoundController.play_sound("player_hurt")
	get_hurt()

func move_or_be_crushed(amount : Vector2, by : PhysicsBody2D) -> void:
	var collision : KinematicCollision2D = move_and_collide(amount)
	# If, for any reason, we can't move, we get crushed
	if collision != null and collision.collider != by:
		get_crushed()

func _physics_process(delta : float) -> void:
	match current_state:
		PLAYER_STATE.ON_GROUND:
			on_ground(delta)
		PLAYER_STATE.IN_AIR:
			in_air(delta)
		PLAYER_STATE.WALL_JUMP:
			wall_jump(delta)
		PLAYER_STATE.HURT:
			hurt(delta)
	# Janky shit
	if position.y > 410 and current_state != PLAYER_STATE.HURT:
		get_hurt()
		SoundController.play_sound("player_fall")

func _on_Timer_NextFrame_timeout() -> void:
	anim_index += 1.0
	update_sprite_frame()
