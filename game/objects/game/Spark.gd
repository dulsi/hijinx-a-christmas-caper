extends Area2D

const MOVEMENT_SPEED : float = 64.0

onready var sprite : Sprite = $Sprite
onready var raycast_up : RayCast2D = $RayCast2D_Up
onready var raycast_down : RayCast2D = $RayCast2D_Down
onready var raycast_left : RayCast2D = $RayCast2D_Left
onready var raycast_right : RayCast2D = $RayCast2D_Right

var current_direction : Vector2
var moving_anticlockwise : bool = false

var anim_index : float = randf()
onready var movement_since_last_check : float = 0.0

func choose_first_direction() -> void:
	if moving_anticlockwise:
		if raycast_right.is_colliding():
			current_direction = Vector2.DOWN
		elif raycast_down.is_colliding():
			current_direction = Vector2.LEFT
		elif raycast_left.is_colliding():
			current_direction = Vector2.UP
		elif raycast_up.is_colliding():
			current_direction = Vector2.RIGHT
		else:
			current_direction = Vector2.ZERO
	else:
		if raycast_right.is_colliding():
			current_direction = Vector2.UP
		elif raycast_down.is_colliding():
			current_direction = Vector2.RIGHT
		elif raycast_left.is_colliding():
			current_direction = Vector2.DOWN
		elif raycast_up.is_colliding():
			current_direction = Vector2.LEFT
		else:
			current_direction = Vector2.ZERO

func is_path_ahead_blocked(which_way : Vector2) -> bool:
	match which_way:
		Vector2.UP:
			return raycast_up.is_colliding()
		Vector2.DOWN:
			return raycast_down.is_colliding()
		Vector2.LEFT:
			return raycast_left.is_colliding()
		Vector2.RIGHT:
			return raycast_right.is_colliding()
	return false

func is_path_beneath(which_way : Vector2) -> bool:
	if moving_anticlockwise:
		match which_way:
			Vector2.UP:
				return raycast_left.is_colliding()
			Vector2.DOWN:
				return raycast_right.is_colliding()
			Vector2.LEFT:
				return raycast_down.is_colliding()
			Vector2.RIGHT:
				return raycast_up.is_colliding()
	else:
		match which_way:
			Vector2.UP:
				return raycast_right.is_colliding()
			Vector2.DOWN:
				return raycast_left.is_colliding()
			Vector2.LEFT:
				return raycast_up.is_colliding()
			Vector2.RIGHT:
				return raycast_down.is_colliding()
	return false

func get_next_directions() -> Array:
	if moving_anticlockwise:
		match current_direction:
			Vector2.UP:
				return [Vector2.LEFT, Vector2.RIGHT, Vector2.DOWN]
			Vector2.DOWN:
				return [Vector2.RIGHT, Vector2.LEFT, Vector2.UP]
			Vector2.LEFT:
				return [Vector2.DOWN, Vector2.UP, Vector2.RIGHT]
			Vector2.RIGHT:
				return [Vector2.UP, Vector2.DOWN, Vector2.LEFT]
	else:
		match current_direction:
			Vector2.UP:
				return [Vector2.RIGHT, Vector2.LEFT, Vector2.UP]
			Vector2.DOWN:
				return [Vector2.LEFT, Vector2.RIGHT, Vector2.DOWN]
			Vector2.LEFT:
				return [Vector2.UP, Vector2.DOWN, Vector2.LEFT]
			Vector2.RIGHT:
				return [Vector2.DOWN, Vector2.UP, Vector2.RIGHT]
	return []

func pick_new_direction() -> Vector2:
	# Janky hack, let's make sure it's lined up properly
	position = position.snapped(Vector2(16, 16))
	# Okay, now do the thing
	if not is_path_beneath(current_direction):
		var next_directions : Array = get_next_directions()
		for direction in next_directions:
			if not is_path_ahead_blocked(direction):
				return direction
	elif is_path_ahead_blocked(current_direction):
		var next_directions : Array = get_next_directions()
		for direction in next_directions:
			if not is_path_ahead_blocked(direction):
				return direction
	return current_direction

func _process(delta : float) -> void:
	anim_index += delta
	sprite.frame = wrapi(anim_index * 10.0, 0, sprite.hframes)
	position += current_direction * MOVEMENT_SPEED * delta
	movement_since_last_check += MOVEMENT_SPEED * delta
	if movement_since_last_check >= Constants.TILE_SIZE.x:
		movement_since_last_check -= Constants.TILE_SIZE.x
		position -= current_direction * movement_since_last_check
		current_direction = pick_new_direction()
		movement_since_last_check = 0.0
	# Oh bollocks to it
	if current_direction == Vector2.ZERO:
		choose_first_direction()

func _body_entered(body : Node) -> void:
	if body is Player:
		body.get_hurt_by_spark()

func _ready() -> void:
	choose_first_direction()
