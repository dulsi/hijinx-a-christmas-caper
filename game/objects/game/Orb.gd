extends Area2D

class_name Orb

const _OrbEnergy : PackedScene = preload("res://objects/game/OrbEnergy.tscn")
const _OrbSparkles : PackedScene = preload("res://objects/game/OrbSparkles.tscn")

onready var sprite : Sprite = $Sprite
onready var sprite_lit : Sprite = $Sprite_Lit
onready var tween : Tween = $Tween

var which_colour : int

enum STATE {INACTIVE, READY, ACTIVE}

onready var state : int = STATE.INACTIVE

func set_colour(which : int) -> void:
	which_colour = which
	sprite.frame = which + 8
	sprite_lit.frame = which + 4

func make_ready(which : int) -> void:
	if which == which_colour:
		state = STATE.READY
		get_tree().call_group("orb_sparkles", "set_target_position", global_position, which_colour)
		# If we're the red orb, make the sparkles
		if which == 0:
			var sparkles : Node2D = _OrbSparkles.instance()
			sparkles.global_position = global_position
			get_parent().add_child(sparkles)

func pulse() -> void:
	tween.interpolate_property(sprite_lit, "modulate", Color.white, Color.transparent, 0.5)
	tween.start()

func pulse_in_sequence() -> void:
	yield(get_tree().create_timer(1.0 + (which_colour / 2.0)), "timeout")
	pulse()

func release_energy_in_sequence() -> void:
	yield(get_tree().create_timer(1.0 + (which_colour / 2.0)), "timeout")
	var energy : Sprite = _OrbEnergy.instance()
	energy.global_position = global_position
	energy.colour = which_colour
	get_parent().add_child(energy)
	pulse()
	sprite.frame = which_colour + 8

func _body_entered(body : PhysicsBody2D) -> void:
	if body is Player and state == STATE.READY:
		pulse()
		sprite.frame = which_colour
		state = STATE.ACTIVE
		get_tree().call_group("orb_controller", "orb_touched", which_colour)

func _ready() -> void:
	pulse_in_sequence()
