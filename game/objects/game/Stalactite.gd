extends KinematicBody2D

class_name Stalactite

const MAX_FALL_SPEED : float = 512.0
const FALL_INCR : float = 512.0

onready var sprite : Sprite = $Sprite
onready var raycast : RayCast2D = $RayCast2D

enum STATE {HANGING, FALLING}

var current_state : int = STATE.HANGING
var velocity : Vector2 = Vector2.DOWN

func move_or_be_crushed(amount : Vector2, by : PhysicsBody2D) -> void:
	move_and_collide(amount)

func hang(delta : float) -> void:
	if raycast.is_colliding():
		var collider : Node2D = raycast.get_collider()
		if collider is Player:
			current_state = STATE.FALLING
			velocity.y = MAX_FALL_SPEED / 8.0
			sprite.frame = 1
			SoundController.play_sound("stalactite_fall")

func fall(delta : float) -> void:
	if test_move(transform, Vector2.DOWN) == false:
		velocity.y = clamp(velocity.y + (FALL_INCR * delta), 0, MAX_FALL_SPEED)
	var collision : KinematicCollision2D = move_and_collide(velocity * delta)
	if collision != null:
		var collider : Node2D = collision.collider
		if collider is Player and velocity.y > 16.0:
			collider.get_hurt_by_stalactite()
		elif collision.normal == Vector2.UP:
			if velocity.y > 128.0:
				SoundController.play_sound("stalactite_land")
			velocity.y = 0

func _process(delta : float) -> void:
	match current_state:
		STATE.HANGING:
			hang(delta)
		STATE.FALLING:
			fall(delta)
