extends Node2D

onready var sky : Sprite = $Sky
onready var skyline : Sprite = $Skyline
onready var mountains : Sprite = $Mountains

func set_palette(palette : int) -> void:
	sky.frame = palette
	skyline.material.set_shader_param("which_palette", palette)
	mountains.material.set_shader_param("which_palette", palette)

func set_mountains(which : int) -> void:
	mountains.frame = which
