extends Sprite

class_name OrbMarker

var board_position : Vector2
onready var colour : int = 0

func get_hints() -> String:
	return "(Z) Change colour/order"

func change_property() -> void:
	colour = wrapi(colour + 1, 0, 4)
	frame = colour

func change_property_alt() -> void:
	pass

func get_object_data() -> Dictionary:
	return {
		"type": Constants.EDITOR_ITEM_NAME_ORB,
		"position": [board_position.x, board_position.y],
		"colour": colour
	}
