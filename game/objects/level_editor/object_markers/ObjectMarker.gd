extends Sprite

class_name ObjectMarker

var which_object : int
var board_position : Vector2

func get_hints() -> String:
	return ""

func change_property() -> void:
	pass

func change_property_alt() -> void:
	pass

func _ready() -> void:
	frame = which_object

func get_object_data() -> Dictionary:
	return {
		"type": Constants.ITEM_ID_TO_LABEL[which_object],
		"position": [board_position.x, board_position.y]
	}
