extends Sprite

class_name SparkMarker

var board_position : Vector2
onready var moving_anticlockwise : bool = false

func get_hints() -> String:
	return "(Z) Change rotation (currently %s)" % ("anticlockwise" if moving_anticlockwise else "clockwise")

func change_property() -> void:
	moving_anticlockwise = !moving_anticlockwise

func change_property_alt() -> void:
	pass

func get_object_data() -> Dictionary:
	return {
		"type": Constants.EDITOR_ITEM_NAME_SPARK,
		"position": [board_position.x, board_position.y],
		"anticlockwise": moving_anticlockwise
	}
