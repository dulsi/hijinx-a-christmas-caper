extends Sprite

class_name MovingPlatformMarker

var board_position : Vector2
onready var direction : Vector2 = Vector2.RIGHT

func get_hints() -> String:
	return "(Z) Starting direction = %s" % ("right" if direction == Vector2.RIGHT else "left")

func change_property() -> void:
	direction *= -1

func change_property_alt() -> void:
	pass

func get_object_data() -> Dictionary:
	return {
		"type": Constants.EDITOR_ITEM_NAME_MOVING_PLATFORM,
		"position": [board_position.x, board_position.y],
		"direction": [direction.x, direction.y]
	}
