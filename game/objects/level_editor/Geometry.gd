extends TileMap

const _ObjectMarker : PackedScene = preload("res://objects/level_editor/object_markers/ObjectMarker.tscn")
const _SpikeMarker : PackedScene = preload("res://objects/level_editor/object_markers/SpikeMarker.tscn")
const _MovingPlatformMarker : PackedScene = preload("res://objects/level_editor/object_markers/MovingPlatformMarker.tscn")
const _SparkMarker : PackedScene = preload("res://objects/level_editor/object_markers/SparkMarker.tscn")
const _OrbMarker : PackedScene = preload("res://objects/level_editor/object_markers/OrbMarker.tscn")
const _HammerMarker : PackedScene = preload("res://objects/level_editor/object_markers/HammerMarker.tscn")
const _SpringboardPlatformMarker : PackedScene = preload("res://objects/level_editor/object_markers/SpringboardPlatformMarker.tscn")
const _TurretMarker : PackedScene = preload("res://objects/level_editor/object_markers/TurretMarker.tscn")
const _CarlMarker : PackedScene = preload("res://objects/level_editor/object_markers/CarlMarker.tscn")

var player_spawn_pos : Vector2 = Vector2(-1, -1)
var goal_hidden_by_orbs : bool = false

func add_dirt_tile(pos : Vector2) -> void:
	set_cellv(pos, Constants.TILEMAP_DIRT)

func add_ice_tile(pos : Vector2) -> void:
	set_cellv(pos, Constants.TILEMAP_ICE)

func add_brick_tile(pos : Vector2) -> void:
	set_cellv(pos, Constants.TILEMAP_BRICK)

func add_player(pos : Vector2) -> void:
	player_spawn_pos = pos

func add_object(pos : Vector2, which_object : String) -> void:
	var object_marker : Node2D = _ObjectMarker.instance()
	object_marker.which_object = Constants.ITEM_LABEL_TO_ID[which_object]
	object_marker.board_position = pos
	object_marker.position = pos * Constants.TILE_SIZE
	add_child(object_marker)

func add_spike(pos : Vector2, frame : int) -> void:
	var object_marker : Node2D = _SpikeMarker.instance()
	object_marker.board_position = pos
	object_marker.position = pos * Constants.TILE_SIZE
	add_child(object_marker)
	object_marker.frame = frame

func add_spark(pos : Vector2, anticlockwise : bool) -> void:
	var spark_marker : Node2D = _SparkMarker.instance()
	spark_marker.board_position = pos
	spark_marker.position = pos * Constants.TILE_SIZE
	add_child(spark_marker)
	spark_marker.moving_anticlockwise = anticlockwise

func add_moving_platform(pos : Vector2, initial_direction : Vector2) -> void:
	var object_marker : Node2D = _MovingPlatformMarker.instance()
	object_marker.board_position = pos
	object_marker.position = pos * Constants.TILE_SIZE
	add_child(object_marker)
	object_marker.direction = initial_direction

func add_orb(pos : Vector2, colour : int) -> void:
	var object_marker : Node2D = _OrbMarker.instance()
	object_marker.board_position = pos
	object_marker.position = pos * Constants.TILE_SIZE
	add_child(object_marker)
	object_marker.frame = colour
	object_marker.colour = colour

func add_hammer(pos : Vector2, starting_delay : float) -> void:
	var object_marker : Node2D = _HammerMarker.instance()
	object_marker.board_position = pos
	object_marker.position = pos * Constants.TILE_SIZE
	add_child(object_marker)
	object_marker.starting_delay = starting_delay

func add_springboard_platform(pos : Vector2, initial_direction : Vector2) -> void:
	var object_marker : Node2D = _SpringboardPlatformMarker.instance()
	object_marker.board_position = pos
	object_marker.position = pos * Constants.TILE_SIZE
	add_child(object_marker)
	object_marker.direction = initial_direction

func add_turret(pos : Vector2, firing_direction : Vector2, firing_interval : float) -> void:
	var object_marker : Node2D = _TurretMarker.instance()
	object_marker.board_position = pos
	object_marker.position = pos * Constants.TILE_SIZE
	add_child(object_marker)
	object_marker.set_firing_direction(firing_direction)
	object_marker.set_firing_interval(firing_interval)

func add_carl(pos : Vector2, which_conversation : String) -> void:
	var object_marker : Node2D = _CarlMarker.instance()
	object_marker.board_position = pos
	object_marker.position = pos * Constants.TILE_SIZE
	object_marker.which_conversation = which_conversation
	add_child(object_marker)

func get_tile_type_at(pos : Vector2) -> int:
	return get_cellv(pos)

func get_objects() -> Array:
	var objects : Array = []
	for object in get_tree().get_nodes_in_group("object_marker"):
		objects.append(object.get_object_data())
	return objects

func get_flags() -> Array:
	var flags : Array = []
	if goal_hidden_by_orbs:
		flags.append(Constants.LEVEL_PROPERTY_NAME_GOAL_HIDDEN_BY_ORBS)
	return flags

func add_orb_controller() -> void:
	goal_hidden_by_orbs = true

func update_bitmasks() -> void:
	update_bitmask_region()

func update_snow_tiles() -> void:
	pass

func clear_level() -> void:
	clear()
	for object in get_children():
		object.queue_free()
	player_spawn_pos = Vector2(-1, -1)
