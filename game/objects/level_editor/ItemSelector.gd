extends Control

const Icons : StreamTexture = preload("res://sprites/level_editor_icons.png")

var selected_object : int = 0
var icon_offset : float = 0

func _draw() -> void:
	for i in range(-1, 6):
		var object_index : int = wrapi(i + selected_object, 0, Constants.ITEM_COUNT)
		var icon_pos_on_sheet : Vector2 = Vector2(
			object_index % int(Constants.EDITOR_ICON_SHEET_SIZE.x),
			object_index / int(Constants.EDITOR_ICON_SHEET_SIZE.x)
		)
		var icon_rect : Rect2 = Rect2(icon_pos_on_sheet * Constants.TILE_SIZE, Constants.TILE_SIZE)
		var icon_dest : Rect2 = Rect2((i + icon_offset) * 64, 0, 32, 32)
		var alpha : float = max(1 - (max(icon_dest.position.x - 256, 0) / 64.0), 0)
		var colour : Color = Color(1.0, 1.0, 1.0, alpha)
		
		draw_texture_rect_region(Icons, icon_dest, icon_rect, colour)
	draw_rect(Rect2(Vector2(-5, -5), Vector2(42, 42)), Color.white, false, 4.0)

func _input(event : InputEvent) -> void:
	if event.is_action_pressed("editor_next_object"):
		selected_object = wrapi(selected_object + 1, 0, Constants.ITEM_COUNT)
		icon_offset += 1.0
		update()
	elif event.is_action_pressed("editor_previous_object"):
		selected_object = wrapi(selected_object - 1, 0, Constants.ITEM_COUNT)
		icon_offset -= 1.0
		update()

func _process(delta : float) -> void:
	icon_offset = lerp(icon_offset, 0, delta * 10.0)
	update()
