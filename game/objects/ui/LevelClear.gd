extends Control

onready var label_level_clear : Label = $Label_LevelClear
onready var grid_stats : GridContainer = $Grid_Stats
onready var label_time_value : Label = $Grid_Stats/Label_Time_Value
onready var label_comparison_label : Label = $Grid_Stats/Label_Comparison_Label
onready var label_comparison_value : Label = $Grid_Stats/Label_Comparison_Value
onready var label_got_rings : Label = $VBox_Messages/Label_GotRings
onready var label_new_best : Label = $VBox_Messages/Label_NewBest
onready var background : ColorRect = $ColorRect_Background
onready var center : Control = $Center
onready var menu : Control = $Center/Menu
onready var tween : Tween = $Tween

signal goto_next_level
signal restart_level
signal back_to_title

func transition_in() -> void:
	label_level_clear.rect_position.x = -200
	grid_stats.rect_position.x = 640
	background.modulate = Color.transparent
	tween.interpolate_property(label_level_clear, "rect_position", Vector2(-200, 128),  Vector2(237, 128), 0.75, Tween.TRANS_QUINT, Tween.EASE_OUT)
	tween.interpolate_property(grid_stats, "rect_position", Vector2(640, 164),  Vector2(250, 164), 0.75, Tween.TRANS_QUINT, Tween.EASE_OUT)
	tween.interpolate_property(center, "rect_position", Vector2(0, 320),  Vector2(0, 180), 0.75, Tween.TRANS_QUINT, Tween.EASE_OUT)
	tween.interpolate_property(background, "modulate", Color.transparent, Color.white, 0.25)
	tween.start()
	show()
	menu.active = true
	menu.current_child = 0
	menu.move_cursor()
	SoundController.play_sound("level_clear")

func set_level_stats(clear_time : int, par_time : int, dev_best : int, player_best : int, got_rings : int) -> void:
	label_time_value.text = Utilities.time_to_string(clear_time)
	label_got_rings.hide()
	label_new_best.hide()
	# What comparison are we using?
	label_comparison_label.text = "Par:"
	label_comparison_value.text = Utilities.time_to_string(par_time)
	if clear_time < par_time and player_best != -1:
		if player_best <= clear_time:
			label_comparison_label.text = "Your best:"
			label_comparison_value.text = Utilities.time_to_string(player_best)
		else:
			label_comparison_label.text = "Old best:"
			label_comparison_value.text = Utilities.time_to_string(player_best)
			label_new_best.show()
	if got_rings:
		label_got_rings.show()

func _ready() -> void:
	menu.items = {
		"level_clear": {
			"type": "menu",
			"children": ["continue", "restart", "back_to_title"]
		},
		"continue": {
			"type": "button",
			"label": "Continue"
		},
		"restart": {
			"type": "button",
			"label": "Restart Level"
		},
		"back_to_title": {
			"type": "button",
			"label": "Back to Title Screen"
		}
	}
	menu.current_item = "level_clear"
	menu.resize(true)
	menu.active = false

func _on_Menu_button_pressed(slug : String) -> void:
	match slug:
		"continue":
			emit_signal("goto_next_level")
			menu.active = false
		"restart":
			emit_signal("restart_level")
			menu.active = false
		"back_to_title":
			emit_signal("back_to_title")
			menu.active = false
