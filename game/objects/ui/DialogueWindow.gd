extends Control

const MugshotCarl : StreamTexture = preload("res://sprites/convo/carl.png")
const MugshotJinx : StreamTexture = preload("res://sprites/convo/jinx.png")

onready var rect_panel : ColorRect = $Rect_Panel
onready var texture_mugshot : TextureRect = $Rect_Panel/TextureRect_Mugshot
onready var label_dialogue : Label = $Rect_Panel/Label_Dialogue
onready var timer_next_character : Timer = $Timer_NextCharacter
onready var tween : Tween = $Tween

enum STATE {IDLE, TYPING, TYPED}

onready var current_state : int = STATE.IDLE
onready var current_conversation : String = ""
onready var current_line_number : int = -1
onready var current_character : String = ""
onready var active : bool = false

signal conversation_finished

func transition_in() -> void:
	tween.interpolate_property(rect_panel, "rect_position", Vector2(0, 300), Vector2(0, 240), 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tween.interpolate_property(texture_mugshot, "rect_position", Vector2(-48, -84), Vector2(16, -24), 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tween.interpolate_property(rect_panel, "modulate", Color.transparent, Color.white, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tween.start()

func transition_out() -> void:
	tween.interpolate_property(rect_panel, "rect_position", Vector2(0, 240), Vector2(0, 300), 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tween.interpolate_property(texture_mugshot, "rect_position", Vector2(16, -24), Vector2(-48, -84), 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tween.interpolate_property(rect_panel, "modulate", Color.white, Color.transparent, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	tween.start()

func change_mugshot(speaker : String) -> void:
	match speaker:
		"carl":
			texture_mugshot.texture.atlas = MugshotCarl
		"jinx":
			texture_mugshot.texture.atlas = MugshotJinx

func change_expression(which : int) -> void:
	texture_mugshot.texture.region.position.x = which * 32

func play_line(conversation_name : String, line_number : int) -> void:
	current_character = Conversations.get_speaker_for_line(conversation_name, line_number)
	change_mugshot(current_character)
	change_expression(Conversations.get_expression_for_line(conversation_name, line_number))
	label_dialogue.text = Conversations.get_dialogue_in_line(conversation_name, line_number)
	label_dialogue.visible_characters = 0
	current_state = STATE.TYPING
	# Music flags
	if Conversations.line_has_flag(current_conversation, current_line_number, Conversations.FLAG_LINE_NO_MUSIC):
		MusicController.stop_music()
	elif Conversations.line_has_flag(current_conversation, current_line_number, Conversations.FLAG_LINE_MUSIC_CUE_A):
		MusicController.play_music("Take My Hand")
	# Type speed flags
	if Conversations.line_has_flag(current_conversation, current_line_number, Conversations.FLAG_LINE_SLOW):
		timer_next_character.wait_time = 0.4
	elif Conversations.line_has_flag(current_conversation, current_line_number, Conversations.FLAG_LINE_FAST):
		timer_next_character.wait_time = 0.01
	else:
		timer_next_character.wait_time = 0.05
	timer_next_character.start()

func line_finished() -> void:
	current_line_number += 1
	if Conversations.does_conversation_have_another_line(current_conversation, current_line_number):
		# This conversation isn't over yet - let's keep going
		play_line(current_conversation, current_line_number)
	else:
		# The conversation is finished - transfer control back to the game and transition out
		transition_out()
		emit_signal("conversation_finished", current_conversation)
		active = false

func skip_to_end_of_line() -> void:
	timer_next_character.stop()
	current_state = STATE.TYPED
	label_dialogue.percent_visible = 1.0

func play_conversation(conversation_name : String) -> void:
	transition_in()
	active = true
	current_conversation = conversation_name
	current_line_number = 0
	play_line(current_conversation, current_line_number)

func _input(event : InputEvent) -> void:
	if not active: return
	if Input.is_action_just_pressed("ui_accept"):
		Input.action_release("jump")
		if current_state == STATE.TYPING:
			skip_to_end_of_line()
		elif current_state == STATE.TYPED:
			line_finished()

func _on_Timer_NextCharacter_timeout() -> void:
	label_dialogue.visible_characters += 1
	if label_dialogue.percent_visible >= 1.0:
		timer_next_character.stop()
		current_state = STATE.TYPED
		# In certain cases, we want to immediate play the next line
		if Conversations.line_has_flag(current_conversation, current_line_number, Conversations.FLAG_LINE_AUTO):
			line_finished()
	# Babble bloops!
	match current_character:
		"carl":
			SoundController.play_sound("babble_carl")
		"jinx":
			SoundController.play_sound("babble_jinx")

func _enter_tree() -> void:
	$Rect_Panel.modulate = Color.transparent
