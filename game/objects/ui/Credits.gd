extends Control

const FONT_PARAGRAPH = preload("res://fonts/paragraph.tres")
const FONT_PARAGRAPH_BOLD = preload("res://fonts/paragraph_bold.tres")

onready var active : bool = false

signal close_credits

func draw_text_with_shadow(font : Font, text : String, position : Vector2, colour : Color, centered : bool = false) -> void:
	var offset : Vector2 = Vector2.ZERO
	if centered:
		offset.x -= font.get_string_size(text).x / 2.0
	draw_string(font, position + offset + Vector2.ONE, text, Constants.COLOUR_SHADOW)
	draw_string(font, position + offset, text, colour)

func draw_role(position : Vector2, role : String) -> void:
	var role_width : float = FONT_PARAGRAPH.get_string_size(role).x
	draw_text_with_shadow(FONT_PARAGRAPH, role, position + Vector2(- role_width - 8, 0), Constants.COLOUR_SHADE_A)

func draw_name(position : Vector2, name : String, other : String = "") -> void:
	var name_width : float = FONT_PARAGRAPH.get_string_size(name).x
	var other_width : float = FONT_PARAGRAPH_BOLD.get_string_size(other).x
	draw_text_with_shadow(FONT_PARAGRAPH, name, position + Vector2(0, 0), Constants.COLOUR_BRIGHT)
	draw_text_with_shadow(FONT_PARAGRAPH_BOLD, other, position + Vector2(0, 14), Constants.COLOUR_SHADE_A)

func draw_credit(position : Vector2, role : String, name : String, other : String = "") -> void:
	draw_role(position, role)
	draw_name(position, name, other)

func _draw() -> void:
	draw_credit(Vector2(220, 50), "Game by", "John Gabriel", "@JohnGabrielUK")
	draw_credit(Vector2(220, 90), "Art by", "Jerico Landry", "@D4yz_Ag0")
	draw_credit(Vector2(220, 130), "Sound design by", "Vidar Leimar", "@LeimSound")
	draw_credit(Vector2(220, 170), "Playtesting by", "Will Bradley  &", "@Bradinator64")
	draw_name(Vector2(336, 170), "Karlo Koscal", "@WingedAdventurer")
	draw_credit(Vector2(220, 210), "Cover art by", "Fadil Rahadiansyah", "@beatheck")
	draw_credit(Vector2(220, 250), "Music by", "One Man Symphony", "onemansymphony.bandcamp.com")
	draw_credit(Vector2(220, 290), "Made with", "Godot Engine 3.2.3", "Licensed under MIT: http://godotengine.org/license")
	draw_text_with_shadow(FONT_PARAGRAPH_BOLD, "(ESC) Go back", Vector2(320, 348), Constants.COLOUR_SHADE_A, true)

func _input(event : InputEvent) -> void:
	if not active: return
	if event.is_action_pressed("ui_cancel"):
		SoundController.play_sound("ui_back")
		emit_signal("close_credits")
