extends Control

onready var texture_icon : TextureRect = $TextureRect_Icon
onready var label_a : Label = $LabelA
onready var label_b : Label = $LabelB
onready var tween : Tween = $Tween

var achievements : Dictionary

func _process(delta : float) -> void:
	if not GameProgress.is_achievement_unlocked("highground"):
		# Is the player above the stage, and standing?
		for node in get_tree().get_nodes_in_group("player"):
			if node.global_position.y < -20 and node.current_state == Player.PLAYER_STATE.ON_GROUND:
				do_popup("highground")

func all_optional_conversations_had() -> bool:
	for convo_name in Conversations.list_optional_conversations():
		if not GameProgress.conversations_had.has(convo_name):
			return false
	return true

func conversation_finished(conversation_name : String) -> void:
	if not GameProgress.is_achievement_unlocked("talkative"):
		if all_optional_conversations_had():
			do_popup("talkative")

func level_finished(level_name : String, clear_time : float) -> void:
	if not GameProgress.is_achievement_unlocked("lightfooted"):
		if level_name == "a5":
			var crumblers : Array = get_tree().get_nodes_in_group("crumbler")
			if crumblers.size() >= 16:
				do_popup("lightfooted")
	if not GameProgress.is_achievement_unlocked("completionist"):
		if GameProgress.get_completion_percentage() >= 1.0:
			do_popup("completionist")
	if not GameProgress.is_achievement_unlocked("overtaken"):
		if clear_time < Levels.get_level_dev_best(level_name):
			do_popup("overtaken")

func do_popup(achievement_name : String) -> void:
	if tween.is_active():
		yield(tween, "tween_all_completed")
	var achievement_data : Dictionary = achievements[achievement_name]
	label_b.text = achievement_data["name"]
	texture_icon.texture.region.position = Vector2(32 * achievement_data["icon"], 0)
	tween.interpolate_property(self, "rect_position", Vector2(640, 8), Vector2(420, 8), 0.75, Tween.TRANS_BOUNCE, Tween.EASE_OUT)
	tween.interpolate_property(self, "rect_position", Vector2(420, 8), Vector2(640, 8), 0.5, Tween.TRANS_CUBIC, Tween.EASE_IN, 1.75)
	tween.start()
	SoundController.play_sound("achievement_unlocked")
	GameProgress.set_achievement_unlocked(achievement_name)
	GameProgress.save_game()

func _ready() -> void:
	achievements = Utilities.load_data_from_json("res://data/achievements.json")
