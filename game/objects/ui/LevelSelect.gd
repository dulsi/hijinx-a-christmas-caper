extends Control

const FONT_HEADING : Font = preload("res://fonts/heading.tres")
const FONT_PARAGRAPH : Font = preload("res://fonts/paragraph_bold.tres")
const FONT_SMALL : Font = preload("res://fonts/paragraph_small.tres")
const FONT_TIMER : Font = preload("res://fonts/timer.tres")

const SPRITE_CURSOR : StreamTexture = preload("res://sprites/ui/cursor.png")
const SPRITE_ICONS : StreamTexture = preload("res://sprites/ui/icons.png")

const LEVEL_HEIGHT : float = 32.0

onready var selected_set_index : int = 0
onready var selected_level : int = 0
onready var cursor_position : float = 0.0
onready var cursor_position_target : float = 0.0
onready var set_position : float = 0.0
onready var set_position_target : float = 0.0
onready var set_scroll : float = 0.0
onready var set_scroll_target : float = 0.0

onready var active : bool = false

signal level_selected
signal close_level_select

func get_selected_set_name() -> String:
	return Levels.sets.keys()[selected_set_index]

func draw_text_with_shadow(font : Font, text : String, position : Vector2, colour : Color, centered : bool = false) -> void:
	var offset : Vector2 = Vector2.ZERO
	if centered:
		offset.x -= font.get_string_size(text).x / 2.0
	draw_string(font, position + offset + Vector2.ONE, text, Constants.COLOUR_SHADOW)
	draw_string(font, position + offset, text, colour)

func draw_level_locked(index : int, position : Vector2) -> void:
	var number_width : float = FONT_PARAGRAPH.get_string_size("%d: " % (index+1)).x
	draw_text_with_shadow(FONT_PARAGRAPH, "%d: " % (index+1), position + Vector2(34 - number_width, 0), Constants.COLOUR_SHADE_B)
	draw_text_with_shadow(FONT_PARAGRAPH, "Locked", position + Vector2(34, 0), Constants.COLOUR_SHADE_B)

func draw_level_unlocked(level_name : String, index : int, position : Vector2) -> void:
	# Colours
	var colour_a : Color = Constants.COLOUR_BRIGHT if index == selected_level else Constants.COLOUR_SHADE_A
	var colour_b : Color = Constants.COLOUR_SHADE_A if index == selected_level else Constants.COLOUR_SHADE_B
	# Progress
	var best_time : int = GameProgress.get_best_time_for_level(level_name)
	# Strings
	var title : String = Levels.get_level_title(level_name)
	var subtitle : String = Levels.get_level_subtitle(level_name)
	var best_time_string : String = "Best: %s" % (Utilities.time_to_string(best_time) if best_time != -1 else "--:--.---")
	# Widths
	var number_width : float = FONT_PARAGRAPH.get_string_size("%d: " % (index+1)).x
	var best_width : float = FONT_TIMER.get_string_size(best_time_string).x
	# Draw! (pew pew)
	draw_text_with_shadow(FONT_PARAGRAPH, "%d: " % (index+1), position + Vector2(34 - number_width, 0), colour_a)
	draw_text_with_shadow(FONT_PARAGRAPH, title, position + Vector2(34, 0), colour_a)
	draw_text_with_shadow(FONT_SMALL, subtitle, position + Vector2(34, 12), colour_b)
	draw_text_with_shadow(FONT_TIMER, best_time_string, position + Vector2(480 - best_width, 12), colour_b)
	# Progress icons
	var cursor : Vector2 = Vector2.ZERO
	if GameProgress.get_best_time_for_level(level_name) <= Levels.get_level_par(level_name) and GameProgress.get_best_time_for_level(level_name) != -1:
		draw_texture_rect_region(SPRITE_ICONS, Rect2(position + cursor + Vector2(468, -10), Vector2(12, 12)), Rect2(24, 0, 12, 12))
		cursor.x -= 14
	if GameProgress.is_level_cleared_with_rings(level_name):
		draw_texture_rect_region(SPRITE_ICONS, Rect2(position + cursor + Vector2(468, -10), Vector2(12, 12)), Rect2(12, 0, 12, 12))
		cursor.x -= 16
	if GameProgress.is_level_cleared(level_name):
		draw_texture_rect_region(SPRITE_ICONS, Rect2(position + cursor + Vector2(468, -10), Vector2(12, 12)), Rect2(0, 0, 12, 12))

func draw_level(level_name : String, index : int, position : Vector2) -> void:
	if GameProgress.is_level_unlocked(level_name):
		draw_level_unlocked(level_name, index, position)
	else:
		draw_level_locked(index, position)

func draw_level_set(set_name : String, position : Vector2) -> void:
	var levels : Array = Levels.get_levels_in_set(set_name)
	var cursor : Vector2 = position
	cursor.x += set_position
	cursor.y += set_scroll
	var index : int = 0
	for level in levels:
		draw_level(level, index, cursor)
		index += 1
		cursor += Vector2(0, LEVEL_HEIGHT)
	draw_texture(SPRITE_CURSOR, Vector2(position.x + set_position, position.y + set_scroll + cursor_position))

func draw_header_and_footer() -> void:
	draw_rect(Rect2(0, 0, 640, 64), Color.black)
	draw_rect(Rect2(0, 328, 640, 360), Color.black)
	draw_rect(Rect2(0, 63, 640, 1), Constants.COLOUR_SHADE_B)
	draw_rect(Rect2(0, 328, 640, 1), Constants.COLOUR_SHADE_B)
	# Strings
	var title : String = Levels.get_level_set_title(get_selected_set_name())
	var subtitle : String = Levels.get_level_set_subtitle(get_selected_set_name())
	var completion_percentage : String = "%.1f%%" % (GameProgress.get_completion_percentage() * 100.0)
	var completion_percentage_width : float = FONT_PARAGRAPH.get_string_size(completion_percentage).x
	draw_text_with_shadow(FONT_PARAGRAPH, title, Vector2(320 + set_position, 32), Constants.COLOUR_SHADE_A, true)
	draw_text_with_shadow(FONT_SMALL, subtitle, Vector2(320 + set_position, 44), Constants.COLOUR_SHADE_B, true)
	draw_text_with_shadow(FONT_PARAGRAPH, "(ESC) Go back", Vector2(40, 348), Constants.COLOUR_SHADE_A)
	draw_text_with_shadow(FONT_PARAGRAPH, completion_percentage, Vector2(620 - completion_percentage_width, 348), Constants.COLOUR_SHADE_A, true)
	draw_text_with_shadow(FONT_PARAGRAPH, "Total completion:  ", Vector2(535 - completion_percentage_width, 348), Constants.COLOUR_SHADE_B, true)

func _draw() -> void:
	draw_level_set(get_selected_set_name(), Vector2(80, 96))
	draw_header_and_footer()

func _process(delta : float) -> void:
	cursor_position = lerp(cursor_position, cursor_position_target, delta * 20.0)
	set_scroll = lerp(set_scroll, set_scroll_target, delta * 4.0)
	set_position = lerp(set_position, set_position_target, delta * 10.0)
	update()

func change_selected_level(delta : int) -> void:
	SoundController.play_sound("ui_move")
	var level_count : int = Levels.get_number_of_levels_in_set(get_selected_set_name())
	var valid_position : bool = false
	while not valid_position:
		selected_level = wrapi(selected_level + delta, 0, level_count)
		var level_name : String = Levels.get_level_in_set(get_selected_set_name(), selected_level)
		if GameProgress.is_level_unlocked(level_name):
			valid_position = true
	cursor_position_target = (selected_level * LEVEL_HEIGHT) - 8.0
	set_scroll_target = selected_level * -14.0

func change_selected_set(delta : int) -> void:
	var old_index : int = selected_set_index
	var set_count : int = Levels.get_number_of_level_sets()
	var valid_position : bool = false
	while not valid_position:
		selected_set_index = wrapi(selected_set_index + delta, 0, Levels.get_number_of_level_sets())
		var set_name : String = Levels.get_level_set_name_by_index(selected_set_index)
		if GameProgress.is_set_unlocked(set_name):
			valid_position = true
	if selected_set_index != old_index:
		set_position = float(delta) * 128
		SoundController.play_sound("ui_move")
		# Okay, let's just make sure we've unlocked the currently-highlighted level
		var level_name : String = Levels.get_level_in_set(get_selected_set_name(), selected_level)
		if not GameProgress.is_level_unlocked(level_name):
			change_selected_level(-1)

func select_level() -> void:
	emit_signal("level_selected", get_selected_set_name(), selected_level)

func enter_screen() -> void:
	selected_set_index = 0
	selected_level = 0
	cursor_position_target = (selected_level * LEVEL_HEIGHT) - 8.0
	cursor_position = cursor_position_target
	set_scroll_target = selected_level * -14.0
	set_scroll = set_scroll_target

func _input(event : InputEvent) -> void:
	if not active: return
	get_tree().set_input_as_handled()
	if event.is_action_pressed("ui_down"):
		change_selected_level(1)
	elif event.is_action_pressed("ui_up"):
		change_selected_level(-1)
	elif event.is_action_pressed("ui_left"):
		change_selected_set(-1)
	elif event.is_action_pressed("ui_right"):
		change_selected_set(1)
	elif event.is_action_pressed("ui_accept"):
		select_level()
	elif event.is_action_pressed("ui_cancel"):
		SoundController.play_sound("ui_back")
		emit_signal("close_level_select")

func _ready() -> void:
	cursor_position_target = (selected_level * LEVEL_HEIGHT) - 8.0
	set_scroll_target = selected_level * -14.0
