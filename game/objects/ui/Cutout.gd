extends ColorRect

onready var tween : Tween = $Tween

onready var transition_amount : float = 1.0

signal transition_done

func set_center(pos : Vector2) -> void:
	material.set_shader_param("center_position", pos)

func _process(delta : float) -> void:
	material.set_shader_param("transition_amount", transition_amount)

func transition(from : float, to : float, time : float = 1.0) -> void:
	material.set_shader_param("transition_amount", from)
	tween.interpolate_property(self, "transition_amount", from, to, time, Tween.TRANS_LINEAR, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_all_completed")
	emit_signal("transition_done")

func transition_in(time : float = 1.0) -> void:
	transition(0.0, 1.0, time)

func transition_out(time : float = 1.0) -> void:
	transition(1.0, 0.0, time)
